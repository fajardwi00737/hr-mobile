import 'package:absen_online/constant/assets_constant.dart';
import 'package:absen_online/constant/color_constant.dart';
import 'package:absen_online/ui/screen/leave_detail.dart';
import 'package:flutter/material.dart';

class CustomLeaveCard extends StatelessWidget {
  // const CustomLeaveCard({Key key}) : super(key: key);
  final String fromDate,toDate,cutiType,leaveReason,statusCuti;
  final int idLeave;
  CustomLeaveCard(this.idLeave,this.fromDate,this.toDate,this.cutiType,this.statusCuti,this.leaveReason);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context)=>LeaveDetail(idLeave,fromDate,toDate,cutiType,statusCuti,leaveReason)));
      },
      child: Container(
        margin: EdgeInsets.only(left: 16,right: 16,bottom: 16),
        padding: EdgeInsets.only(left: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: statusCuti == "approved" ? Colors.green.shade500: statusCuti == "rejected" ? Colors.red.shade500 : Colors.orange.shade300,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 0,
              blurRadius: 1,
              offset: Offset(1, 1),
            ),
          ],
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(topRight: Radius.circular(12),bottomRight: Radius.circular(12))
          ),
          padding: EdgeInsets.only(left: 16,top: 16,right: 16,bottom: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(child: Text("Type : $cutiType",overflow: TextOverflow.ellipsis,style: TextStyle(color: color_black,fontSize: 15,fontFamily: baseUrlFontsPoppinsSemiBold),)),
                  Container(
                    // height: 40,
                    // width: 150,
                    decoration: BoxDecoration(
                        color: statusCuti == "approved" ? Colors.green.shade500: statusCuti == "rejected" ? Colors.red.shade500 : Colors.orange.shade300,
                        borderRadius: BorderRadius.circular(8)
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
                    child: Text(statusCuti,style: TextStyle(color: Colors.white,fontSize:11,fontFamily: baseUrlFontsPoppinsSemiBold)),
                  )
                ],
              ),
              Container(margin: EdgeInsets.symmetric(vertical: 5),child: Divider(thickness: 1.5,color: Color(0xFFBEBEBE),)),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                    SizedBox(height: 6,),
                    Text("From : $fromDate",style: TextStyle(color: Colors.grey.shade700,fontSize:12,fontFamily: baseUrlFontsPoppinsRegular)),
                    SizedBox(height: 6,),
                    Container(child: Text("To : $toDate",style: TextStyle(color: Colors.grey.shade700,fontSize:12,fontFamily: baseUrlFontsPoppinsRegular))),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
