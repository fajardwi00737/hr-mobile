import 'package:absen_online/bloc/leave_detail_cubit/leave_detail_cubit.dart';
import 'package:absen_online/bloc/leave_log/leave_log_cubit.dart';
import 'package:absen_online/constant/assets_constant.dart';
import 'package:absen_online/constant/color_constant.dart';
import 'package:absen_online/ui/screen/auth/login_page.dart';
import 'package:absen_online/ui/support/flushbar/flushbar_notification.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LeaveDetail extends StatefulWidget {
  final String fromDate,toDate,cutiType,leaveReason,statusCuti;
  final int idLeave;
  LeaveDetail(this.idLeave,this.fromDate,this.toDate,this.cutiType,this.statusCuti,this.leaveReason);

  @override
  State<LeaveDetail> createState() => _LeaveDetailState();
}

class _LeaveDetailState extends State<LeaveDetail> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firstTimeOpenThisPage();
  }

  firstTimeOpenThisPage()async{
    print("get data leave");
    context.read<LeaveDetailCubit>().getLeaveDetail(widget.idLeave);
  }

  void listenerLeave(BuildContext context, LeaveDetailState state) {
    if (state is LeaveDetailLoading) {
      print('absen Loading');
    }
    if (state is LeaveDetailFailed) {
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          "Token has expired and can no longer be refreshed",
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      Future.delayed(Duration(seconds: 2), () {
        logout(context);
      });
      // FlushbarNotif.failedBottom(context, "Username atau password salah");

    }
    if (state is LeaveDetailSuccess) {
      // FlushbarNotif.successBottom(context, "Login Berhasil");
      print('absen Succezz');
      // FlushbarNotification.flushbarTop(
      //     context,
      //     FlushbarPosition.BOTTOM,
      //     state.msg,
      //     Colors.white,
      //     color_warning,
      //     Icon(
      //       Icons.check_circle,
      //       color: Colors.white,
      //     ),
      //     false);

      // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
      // Future.delayed(Duration(seconds: 3), () {
      //   Navigator.pushReplacement(context,
      //       new MaterialPageRoute(builder: (context) => HomeNavigation(0)));
      // });
    }
    if (state is LeaveDetailError) {
      print('state error');
      print('state error => '+state.msg);
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      Future.delayed(Duration(seconds: 2), () {
        // logout(context);
      });
    }
  }

  void logout(BuildContext context)async{
    await GeneralSharedPreferences.remove("user_id");
    await GeneralSharedPreferences.remove("token_login");
    await GeneralSharedPreferences.remove("is_login");
    await GeneralSharedPreferences.remove("user_name");
    await GeneralSharedPreferences.remove("user_mail");
    await GeneralSharedPreferences.remove("user_phone");
    await GeneralSharedPreferences.remove("user_address");
    await GeneralSharedPreferences.remove("fcm_token");
    await GeneralSharedPreferences.remove("user_divisi");
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => LoginPage()), (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Leave Status",
          style: TextStyle(
              color: Colors.black
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          color: Colors.black,
          icon: Icon(Icons.arrow_back_rounded,color: Colors.black,),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
      ),
      body: Container(
        margin: EdgeInsets.all(16),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 0,
              blurRadius: 1,
              offset: Offset(1, 1),
            ),
          ],
        ),
        child:BlocConsumer<LeaveDetailCubit,LeaveDetailState>(
          listener: listenerLeave,
          builder: (context,state){
            return Container(
              child: BlocBuilder<LeaveDetailCubit,LeaveDetailState>(
                  builder: (context,state){
                    if(state is LeaveDetailLoading){
                      return Center(
                        child: Container(
                          height: 25,
                          width: 25,
                          child: CircularProgressIndicator(
                            // valueColor: Colors.white,
                            backgroundColor: Colors.blue,
                          ),
                        ),
                      );
                    } else if(state is LeaveDetailSuccess){
                      return Column(

                        children: [
                          SizedBox(height: 16),
                          Text(state.dataLeaveDetail["data"][0]["fullname"],style: TextStyle(color: color_black,fontSize: 17,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),),
                          Text(state.dataLeaveDetail["data"][0]["role"],style: TextStyle(color: Colors.grey.shade600,fontSize: 15,fontFamily: baseUrlFontsPoppinsRegular),),
                          SizedBox(height: 16),
                          Text("Leave Request : ${widget.idLeave} ${state.dataLeaveDetail["data"].length}",style: TextStyle(color: color_black,fontSize: 15,fontFamily: baseUrlFontsPoppinsSemiBold),),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            height: 1,
                            width: MediaQuery.of(context).size.width/3,
                            color: Color(0xFFBEBEBE),
                          ),
                          SizedBox(height: 16),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                SizedBox(width: 120,
                                  child: Column(
                                    children: [
                                      Text("From",style: TextStyle(color:color_grey,fontSize: 15,fontFamily: baseUrlFontsPoppinsRegular),),
                                      Text(widget.fromDate,style: TextStyle(color: color_black,fontSize: 15,fontFamily: baseUrlFontsPoppinsSemiBold),),
                                      SizedBox(
                                        height: 5,
                                      ),

                                    ],
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 10),
                                  height: 40,
                                  width: 1,
                                  color: Color(0xFFBEBEBE),
                                ),
                                SizedBox(
                                  width: 120,
                                  child: Column(
                                    children: [
                                      FittedBox(fit: BoxFit.fitWidth,child: Text("To",style: TextStyle(color: color_grey,fontSize: 15,fontFamily: baseUrlFontsPoppinsRegular),)),
                                      Text(widget.toDate,style: TextStyle(color:color_black,fontSize: 15,fontFamily: baseUrlFontsPoppinsSemiBold),),
                                      SizedBox(
                                        height: 5,
                                      ),

                                    ],
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 16),

                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                SizedBox(
                                  // height: 120,
                                  width: 120,
                                  child: Column(
                                    children: [
                                      Text("Leave Type",style: TextStyle(color:color_grey,fontSize: 15,fontFamily: baseUrlFontsPoppinsRegular),),
                                      Text(widget.cutiType,style: TextStyle(color: color_black,fontSize: 15,fontFamily: baseUrlFontsPoppinsSemiBold),),
                                      SizedBox(
                                        height: 5,
                                      ),

                                    ],
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 10),
                                  height: 40,
                                  width: 1,
                                  color: Color(0xFFBEBEBE),
                                ),
                                SizedBox(
                                  width: 120,
                                  child: Column(
                                    children: [
                                      FittedBox(fit: BoxFit.fitWidth,child: Text("Status",style: TextStyle(color: color_grey,fontSize: 15,fontFamily: baseUrlFontsPoppinsRegular),)),
                                      Text(widget.statusCuti,style: TextStyle(color:color_black,fontSize: 15,fontFamily: baseUrlFontsPoppinsSemiBold),),
                                      SizedBox(
                                        height: 5,
                                      ),

                                    ],
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 16),

                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 16),
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text("Leave Reason",style: TextStyle(color: color_grey,fontSize: 15,fontFamily: baseUrlFontsPoppinsRegular),),
                                Text(widget.leaveReason ?? "-",style: TextStyle(color: Colors.grey.shade600,fontSize: 15,fontFamily: baseUrlFontsPoppinsRegular),),
                              ],
                            ),
                          ),
                          state.dataLeaveDetail["data"].length == 1 ? Container(): authorizeWidget(state)
                        ],
                      );
                    } else if(state is LeaveDetailFailed){
                      print("error cuy");
                      print("error cuy => "+state.msg);

                      return Container(color: Colors.white);
                    } else if(state is LeaveDetailError){
                      print("error cuy");
                      print("error cuy => "+state.msg);

                      return Container(color: Colors.white);
                    }
                    return Container();
                  }
              ),
            );
          },
        )
      ),
    );
  }

  Widget authorizeWidget(LeaveDetailSuccess state){
    return Column(
      children: [
        SizedBox(height: 32),
        Text("Authorized/Approved By",style: TextStyle(color: Colors.grey.shade600,fontSize: 15,fontFamily: baseUrlFontsPoppinsSemiBold),),
        SizedBox(height: 16),
        Text(state.dataLeaveDetail["data"][1]["fullname"],style: TextStyle(color: color_black,fontSize: 17,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),),

        Text(state.dataLeaveDetail["data"][1]["role"],style: TextStyle(color: Colors.grey.shade600,fontSize: 15,fontFamily: baseUrlFontsPoppinsRegular),),
      ],
    );
  }
}
