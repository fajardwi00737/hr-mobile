import 'package:flutter/material.dart';


class CameraStreamPage extends StatefulWidget {
  const CameraStreamPage({Key key}) : super(key: key);

  @override
  State<CameraStreamPage> createState() => _CameraStreamPageState();
}

class _CameraStreamPageState extends State<CameraStreamPage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Camera BottomSheet Example')),
      body: Center(
        child: ElevatedButton(
          onPressed: () {

          },
          child: Text('Open Camera BottomSheet'),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
        },
        child: Icon(Icons.play_arrow),
      ),
    );
  }
}
