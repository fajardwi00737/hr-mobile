import 'package:absen_online/bloc/leave_cubit/leave_reason_cubit.dart';
import 'package:absen_online/constant/color_constant.dart';
import 'package:absen_online/ui/screen/auth/login_page.dart';
import 'package:absen_online/ui/support/flushbar/flushbar_notification.dart';
import 'package:absen_online/ui/widget/card/custom_leave_card.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LeaveList extends StatefulWidget {
  const LeaveList({Key key}) : super(key: key);

  @override
  State<LeaveList> createState() => _LeaveListState();
}

class _LeaveListState extends State<LeaveList> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firstTimeOpenThisPage();
  }

  firstTimeOpenThisPage()async{
    print("get data leave");
    context.read<LeaveReasonCubit>().getLeaveReason();
  }

  void listenerLeave(BuildContext context, LeaveReasonState state) {
    if (state is LeaveReasonLoading) {
      print('absen Loading');
    }
    if (state is LeaveReasonFailed) {
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          "Token has expired and can no longer be refreshed",
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      Future.delayed(Duration(seconds: 2), () {
        logout(context);
      });
      // FlushbarNotif.failedBottom(context, "Username atau password salah");

    }
    if (state is LeaveReasonSuccess) {
      // FlushbarNotif.successBottom(context, "Login Berhasil");
      print('absen Succezz');
      // FlushbarNotification.flushbarTop(
      //     context,
      //     FlushbarPosition.BOTTOM,
      //     state.msg,
      //     Colors.white,
      //     color_warning,
      //     Icon(
      //       Icons.check_circle,
      //       color: Colors.white,
      //     ),
      //     false);

      // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
      // Future.delayed(Duration(seconds: 3), () {
      //   Navigator.pushReplacement(context,
      //       new MaterialPageRoute(builder: (context) => HomeNavigation(0)));
      // });
    }
    if (state is LeaveReasonError) {
      print('state error');
      print('state error => '+state.msg);
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          "Token has expired and can no longer be refreshed",
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      Future.delayed(Duration(seconds: 2), () {
        logout(context);
      });
    }
  }

  void logout(BuildContext context)async{
    await GeneralSharedPreferences.remove("user_id");
    await GeneralSharedPreferences.remove("token_login");
    await GeneralSharedPreferences.remove("is_login");
    await GeneralSharedPreferences.remove("user_name");
    await GeneralSharedPreferences.remove("user_mail");
    await GeneralSharedPreferences.remove("user_phone");
    await GeneralSharedPreferences.remove("user_address");
    await GeneralSharedPreferences.remove("fcm_token");
    await GeneralSharedPreferences.remove("user_divisi");
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => LoginPage()), (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Leave List",
          style: TextStyle(
              color: Colors.black
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          color: Colors.black,
          icon: Icon(Icons.arrow_back_rounded,color: Colors.black,),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
      ),
      body: BlocConsumer<LeaveReasonCubit,LeaveReasonState>(
          listener: listenerLeave,
          builder: (context, state){
            return Container(
                child: BlocBuilder<LeaveReasonCubit,LeaveReasonState>(
                  builder: (context,state){
                    if(state is LeaveReasonLoading){
                      return Center(
                        child: Container(
                          height: 25,
                          width: 25,
                          child: CircularProgressIndicator(
                            // valueColor: Colors.white,
                            backgroundColor: Colors.blue,
                          ),
                        ),
                      );
                    }
                    else if(state is LeaveReasonSuccess){
                      return SingleChildScrollView(
                          child: Container(
                            margin: EdgeInsets.only(top: 16),
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: state.dataLeaveReason['leave'].length,
                                itemBuilder: (context,index){
                                  return CustomLeaveCard(state.dataLeaveReason['leave'][index]["id"],state.dataLeaveReason['leave'][index]["start_date"], state.dataLeaveReason['leave'][index]["end_date"], leaveType(state.dataLeaveReason['type_leave'],state.dataLeaveReason['leave'][index]["leave_type_id"]), state.dataLeaveReason['leave'][index]["status"],state.dataLeaveReason['leave'][index]["leave_reason"]);
                                }
                            )
                          )
                      );
                    }else if (state is LeaveReasonFailed){
                      print("error cuy");
                      print("error cuy => "+state.msg);

                      return Container(color: Colors.white);
                    } else if (state is LeaveReasonError){
                      print("error cuy");
                      print("error cuy => "+state.msg);

                      return Container(color: Colors.white);
                    }
                    return Container();
                  },
                )
            );
          }
      ),
    );
  }

  String leaveType(List<dynamic> typeLeave, int idTypeLeave){
    String leaveTypeString = "";

    for(int i = 0; i < typeLeave.length; i++){
      if(typeLeave[i]["id"] == idTypeLeave){
        // setState(() {
          leaveTypeString = typeLeave[i]["leave_type"];
        // });
      }
    }

    return leaveTypeString;
  }
}
