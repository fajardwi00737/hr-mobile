import 'package:absen_online/bloc/overtime_cubit/overtime_cubit.dart';
import 'package:absen_online/constant/assets_constant.dart';
import 'package:absen_online/constant/color_constant.dart';
import 'package:absen_online/ui/screen/home_navigation.dart';
import 'package:absen_online/ui/support/flushbar/flushbar_notification.dart';
import 'package:absen_online/ui/widget/button/custom_button_confirm.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class OvertimeForm extends StatefulWidget {
  const OvertimeForm({Key key}) : super(key: key);

  @override
  State<OvertimeForm> createState() => _OvertimeFormState();
}

class _OvertimeFormState extends State<OvertimeForm> {
  int overtimeType = 1;
  String selectedValue = "Days";
  TextEditingController te_leave_reason = TextEditingController();
  List<DropdownMenuItem<String>> get dropdownItems{
    List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(child: Text("Days"),value: "Days"),
      DropdownMenuItem(child: Text("Hours"),value: "Hours"),
    ];
    return menuItems;
  }
  List<String> taxAble = ["Taxable", "Non Taxable"];
  String selectedOption;
  int overtimeCount = 1;
  String leaveReason = "";

  String _formatDate2(DateTime dateTime) {
    return DateFormat('MMMM-yyyy').format(dateTime);
  }

  actionConfirmOvertime(BuildContext context)async{
    String _dateStringConfirm = _formatDate2(DateTime.now());
    print("date => "+_dateStringConfirm);
    // print("Tax => "+selectedOption != null? selectedOption:"null");
    print("Overtime type => "+selectedValue);
    print("hour => " +selectedValue == "Hours" ? overtimeCount.toString():"0");
    print("day => " +selectedValue == "Days" ? overtimeCount.toString():"0");

    print("Overtime reason => "+leaveReason);
    print("Overtime amount => $overtimeCount");

    if(leaveReason == "" || selectedOption == null){
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          "Please fill out the form!",
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
    } else {
      // print("reason => "+confirmReason);
      // print("reason => "+leaveReason);
      // print("reason => "+idLeaveReason.toString());
      context.read<OvertimeCubit>().confirmOvertime(selectedValue,overtimeCount, _dateStringConfirm, selectedOption,leaveReason);
    }
    // context.read<AbsenCubit>().actionAbsen(GeneralSharedPreferences.readInt("user_id").toString(), _dateStringConfirm, _timeStringConfirm, selectedValue, userAddress,_position.latitude,_position.longitude);
  }

  void overtimeListener(BuildContext context, OvertimeState state) {
    if (state is OvertimeLoading) {
      print('absen Loading');
    }
    if (state is OvertimeFailed) {
      print('absen Failed');
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_warning,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      // FlushbarNotif.failedBottom(context, "Username atau password salah");

    }
    if (state is OvertimeSuccess) {
      // FlushbarNotif.successBottom(context, "Login Berhasil");
      print('absen Succezz');
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_success,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      Future.delayed(Duration(seconds: 2), () {
        // Navigator.pop(context,true);
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
      });
      // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
      // Future.delayed(Duration(seconds: 3), () {
      //   Navigator.pushReplacement(context,
      //       new MaterialPageRoute(builder: (context) => HomeNavigation(0)));
      // });
    }
    if (state is OvertimeError) {
      print('state error');
      print('state error => '+state.msg);
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Overtime Form"),
      ),
      floatingActionButton: BlocConsumer<OvertimeCubit,OvertimeState>(
        builder: (context, state){
          return CustomButtonConfirm(
            onTap: (){
              actionConfirmOvertime(context);
            },
            title: "Confirm",
            isEnable: true,
            buttonColor: color_primary,
            textColor: Colors.white,
            isLoading: false,
          );
        },
        listener: overtimeListener,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(margin: EdgeInsets.only(left: 16,right: 16,top: 32),child: RichText(
                text: TextSpan(
                  style: new TextStyle(
                      height: 1.1
                  ),
                  children: <TextSpan>[
                    new TextSpan(
                      text: 'Overtime Type',
                      style: TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                    new TextSpan(
                      text: '*',
                      style: new TextStyle(fontSize: 15,color: color_secondary,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                textAlign: TextAlign.center,
              )),

              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 3,
                      offset: Offset(0, 0),
                    ),
                  ],
                ),
                margin: EdgeInsets.all(16),
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                child: DropdownButtonFormField(
                    icon: Icon(Icons.chevron_right),
                    style: TextStyle(
                        fontFamily: baseUrlFontsPoppinsSemiBold,
                        fontSize: 13,
                        color: color_black),
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey.shade300, width: 1),
                        borderRadius: BorderRadius.circular(16),
                      ),
                      filled: true,
                      fillColor: Colors.white,
                    ),
                    dropdownColor: Colors.white,
                    value: selectedValue,
                    onChanged: (newValue) {
                      setState(() {
                        if(selectedValue != newValue){
                          overtimeCount = 1;
                        }

                        selectedValue = newValue;
                        if (selectedValue == "Days") {
                          overtimeType = 1;
                        } else {
                          overtimeType = 2;
                        }
                      });
                    },
                    items: dropdownItems),
              ),
              // Container(margin: EdgeInsets.only(left: 16,right: 16,top: 16),child: RichText(
              //   text: TextSpan(
              //     style: new TextStyle(
              //         height: 1.1
              //     ),
              //     children: <TextSpan>[
              //       new TextSpan(
              //         text: 'Amount of overtime per hour',
              //         style: TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
              //       ),
              //       new TextSpan(
              //         text: '*',
              //         style: new TextStyle(fontSize: 15,color: color_secondary,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
              //       ),
              //     ],
              //   ),
              //   textAlign: TextAlign.center,
              // )),
              // Container(
              //   margin: EdgeInsets.only(left: 16,right: 16,top: 16),
              //   child: Row(
              //     children: [
              //       Container(
              //         decoration: BoxDecoration(
              //           color: Colors.white,
              //           shape: BoxShape.circle,
              //             boxShadow: [BoxShadow(
              //                 color: color_grey
              //             )]
              //         ),
              //         child: IconButton(
              //           onPressed: (){
              //
              //           },
              //           icon: Icon(Icons.remove_rounded),
              //         ),
              //       ),
              //       SizedBox(width: 16),
              //       Text("1",style:TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
              //       SizedBox(width: 16),
              //       Container(
              //         decoration: BoxDecoration(
              //             color: Colors.white,
              //             shape: BoxShape.circle,
              //           boxShadow: [BoxShadow(
              //             color: color_grey
              //           )]
              //         ),
              //         child: IconButton(
              //           onPressed: (){
              //
              //           },
              //           icon: Icon(Icons.add_rounded),
              //         ),
              //       )
              //     ],
              //   ),
              // ),
              Container(margin: EdgeInsets.only(left: 16,right: 16,top: 16),child: RichText(
                text: TextSpan(
                  style: new TextStyle(
                      height: 1.1
                  ),
                  children: <TextSpan>[
                    new TextSpan(
                      text: 'Amount of overtime per ${selectedValue == "Days" ? "day":"hour"}',
                      style: TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                    new TextSpan(
                      text: '*',
                      style: new TextStyle(fontSize: 15,color: color_secondary,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                textAlign: TextAlign.center,
              )),
              Container(
                margin: EdgeInsets.only(left: 16,right: 16,top: 16),
                child: Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          boxShadow: [BoxShadow(
                              color: color_grey
                          )]
                      ),
                      child: IconButton(
                        onPressed: (){
                          if(overtimeCount > 1){
                            setState(() {
                              overtimeCount = overtimeCount - 1;
                            });
                          }
                        },
                        icon: Icon(Icons.remove_rounded),
                      ),
                    ),
                    SizedBox(width: 16),
                    Text("$overtimeCount",style:TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                    SizedBox(width: 16),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          boxShadow: [BoxShadow(
                              color: color_grey
                          )]
                      ),
                      child: IconButton(
                        onPressed: (){
                          setState(() {
                            overtimeCount =overtimeCount + 1;
                          });
                        },
                        icon: Icon(Icons.add_rounded),
                      ),
                    )
                  ],
                ),
              ),
              Container(margin: EdgeInsets.all(16),child: RichText(
                text: TextSpan(
                  style: new TextStyle(
                      height: 1.1
                  ),
                  children: <TextSpan>[
                    new TextSpan(
                      text: 'Tax Overtime',
                      style: TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                    new TextSpan(
                      text: '*',
                      style: new TextStyle(fontSize: 15,color: color_secondary,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                textAlign: TextAlign.center,
              )),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 16),
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 3,
                        offset: Offset(0, 0),
                      ),
                    ],
                  ),
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: taxAble.length,
                    itemBuilder: (context,index){
                      return RadioListTile<String>(
                        title: Text(taxAble[index]),
                        value: taxAble[index],
                        groupValue: selectedOption,
                        onChanged: (value) {
                          setState(() {
                            selectedOption = value;
                          });
                        },
                      );
                    },
                  )
              ),
              Container(margin: EdgeInsets.all(16),child: RichText(
                text: TextSpan(
                  style: new TextStyle(
                      height: 1.1
                  ),
                  children: <TextSpan>[
                    new TextSpan(
                      text: 'Overtime Reason',
                      style: TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                    new TextSpan(
                      text: '*',
                      style: new TextStyle(fontSize: 15,color: color_secondary,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                textAlign: TextAlign.center,
              )),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 3,
                      offset: Offset(0, 0),
                    ),
                  ],
                ),
                margin: EdgeInsets.only(left: 16,right: 16,bottom: 32),
                padding: EdgeInsets.all(16),
                child: TextField(
                  controller: te_leave_reason,
                  style: TextStyle(
                      fontFamily: baseUrlFontsPoppinsRegular),
                  maxLines: 5,
                  maxLength: 150,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Input your overtime reason"
                  ),
                  onChanged: (text){
                    setState(() {
                      leaveReason = text;
                    });
                  },
                  //onSubmitted: _getNotes(),
                ),
              ),

            ],
          ),
        )
      ),
    );
  }
}
