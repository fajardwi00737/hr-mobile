
import 'package:absen_online/bloc/cuti_cubit/cuti_cubit.dart';
import 'package:absen_online/bloc/leave_cubit/leave_reason_cubit.dart';
import 'package:absen_online/constant/assets_constant.dart';
import 'package:absen_online/constant/color_constant.dart';
import 'package:absen_online/ui/screen/home_navigation.dart';
import 'package:absen_online/ui/support/flushbar/flushbar_notification.dart';
import 'package:absen_online/ui/widget/button/custom_button_confirm.dart';
import 'package:absen_online/ui/widget/button/custom_date_picker_form.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class PengajuanCutiPage extends StatefulWidget {
  @override
  _PengajuanCutiPageState createState() => _PengajuanCutiPageState();
}

class _PengajuanCutiPageState extends State<PengajuanCutiPage> {
  TextEditingController fromDateController = TextEditingController();
  TextEditingController toDateController = TextEditingController();
  TextEditingController tf_price_food = TextEditingController();
  TextEditingController te_leave_reason = TextEditingController();
  DateTime _dateTime;
  bool checkboxSakit = false;
  bool checkboxCutiTahunan = false;
  bool checkboxKeperluanKeluarga = false;
  bool checkboxSeminar = false;
  bool checkboxLainnya= false;
  String confirmSelectedFromDate ="",confirmSelectedToDate = "", confirmReason = "";
  int initYears,initMonth,initDay,selectedFromYears,selectedFromMonth,selectedFromdDay,selectedToYears,selectedToMonth,selectedToDay;

  String _formatDate(DateTime dateTime) {
    return DateFormat('dd-MM-yyyy').format(dateTime);
  }

  String _formatDate2(DateTime dateTime) {
    return DateFormat('yyyy-MM-dd').format(dateTime);
  }
  List<String> options = ['Option 1', 'Option 2', 'Option 3'];
  String selectedOption;
  String leaveReason = "";
  int idLeaveReason;
  @override
  void initState() {
    // TODO: implement initState
    initYears = DateTime.now().year;
    initMonth = DateTime.now().month;
    initDay = DateTime.now().day;
    super.initState();
    firstTimeOpenThisPage();
  }

  firstTimeOpenThisPage()async{
    print("get data employee");
    context.read<LeaveReasonCubit>().getLeaveReason();
  }

  void listenerCuti(BuildContext context, CutiState state) {
    if (state is CutiLoading) {
      print('absen Loading');
    }
    if (state is CutiFailed) {
      print('absen Failed');
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_warning,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      // FlushbarNotif.failedBottom(context, "Username atau password salah");

    }
    if (state is CutiSucces) {
      // FlushbarNotif.successBottom(context, "Login Berhasil");
      print('absen Succezz');
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_success,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      Future.delayed(Duration(seconds: 2), () {
        // Navigator.pop(context,true);
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
      });
      // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
      // Future.delayed(Duration(seconds: 3), () {
      //   Navigator.pushReplacement(context,
      //       new MaterialPageRoute(builder: (context) => HomeNavigation(0)));
      // });
    }
    if (state is CutiError) {
      print('state error');
      print('state error => '+state.msg);
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
    }
  }

  actionConfirmCuti(BuildContext context)async{
    print("from date => "+confirmSelectedFromDate);
    print("to Date => "+confirmSelectedToDate);
    // if(checkboxSakit){
    //   confirmReason = "Sakit";
    // } else if(checkboxCutiTahunan) {
    //   confirmReason = "Cuti Tahunan";
    // } else if(checkboxKeperluanKeluarga) {
    //   confirmReason = "Keperluan Keluarga";
    // } else if(checkboxSeminar) {
    //   confirmReason = "Seminar/Pelatihan";
    // } else if(checkboxLainnya) {
    //   confirmReason = tf_price_food.text;
    // } else {
    //   confirmReason = "";
    // }


    if(confirmSelectedFromDate == "" || confirmSelectedToDate == "" || idLeaveReason == null){
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          "Please fill out the form!",
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
    } else {
      // print("reason => "+confirmReason);
      // print("reason => "+leaveReason);
      // print("reason => "+idLeaveReason.toString());
      context.read<CutiCubit>().actionCuti(idLeaveReason,confirmSelectedFromDate, confirmSelectedToDate, leaveReason);
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Leave Form",style: TextStyle(fontFamily: baseUrlFontsPoppinsSemiBold,color: color_black),),
        leading: IconButton(icon: Icon(Icons.arrow_back_rounded,color: Colors.black,), onPressed: (){
          Navigator.pop(context);
        }),
        centerTitle: true,
      ),
      floatingActionButton: BlocConsumer<CutiCubit,CutiState>(
      builder: (context, state){
       return CustomButtonConfirm(
           onTap: (){
             if(state is !CutiLoading){
               actionConfirmCuti(context);
             }
           },
           title: "Confirm",
           isEnable: true,
           buttonColor: color_primary,
           textColor: Colors.white,
           isLoading: (state is CutiLoading) ?true:false,
         );
        },
        listener: listenerCuti,
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 16,),
              // Container(
              //   height: 50,
              //   margin: EdgeInsets.symmetric(horizontal: 16),
              //   width: MediaQuery.of(context).size.width,
              //   padding: EdgeInsets.symmetric(horizontal: 16,vertical: 10),
              //   decoration: BoxDecoration(
              //     borderRadius: BorderRadius.circular(16),
              //     color: Colors.white,
              //     boxShadow: [
              //       BoxShadow(
              //         color: Colors.grey.withOpacity(0.5),
              //         spreadRadius: 1,
              //         blurRadius: 3,
              //         offset: Offset(0, 0),
              //       ),
              //     ], 
              //   ),
              //   child: Row(
              //     children: [
              //       Icon(Icons.mail_outline_rounded),
              //       SizedBox(width: 10,),
              //       Expanded(child: Text(GeneralSharedPreferences.readString("user_mail"),maxLines: 2,overflow: TextOverflow.ellipsis,style: TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),)),
              //     ],
              //   ),
              // ),
              Container(margin: EdgeInsets.all(16),child: RichText(
                text: TextSpan(
                  style: new TextStyle(
                      height: 1.1
                  ),
                  children: <TextSpan>[
                    new TextSpan(
                      text: 'Leave Type',
                      style: TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                    new TextSpan(
                      text: '*',
                      style: new TextStyle(fontSize: 15,color: color_secondary,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                textAlign: TextAlign.center,
              )),
              // Container(margin: EdgeInsets.all(16),child: Text("Leave Type",style: TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),)),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 16),
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 3,
                        offset: Offset(0, 0),
                      ),
                    ],
                  ),
                  child: BlocBuilder<LeaveReasonCubit,LeaveReasonState>(
                    builder: (context,state){
                      if(state is LeaveReasonLoading){
                        return Center(
                          child: Container(
                            height: 25,
                            width: 25,
                            child: CircularProgressIndicator(
                              // valueColor: Colors.white,
                              backgroundColor: Colors.blue,
                            ),
                          ),
                        );
                      } else if(state is LeaveReasonSuccess){
                        // setState(() {
                        //   idLeaveReason = state.dataLeaveReason['type_leave'][0]["id"];
                        //   selectedOption = state.dataLeaveReason['type_leave'][0]["leave_type"];
                        // });
                        return ListView.builder(
                          shrinkWrap: true,
                          itemCount: state.dataLeaveReason['type_leave'].length,
                          itemBuilder: (context,index){
                            return RadioListTile<String>(
                              title: Text(state.dataLeaveReason['type_leave'][index]["leave_type"]),
                              value: state.dataLeaveReason['type_leave'][index]["leave_type"],
                              groupValue: selectedOption,
                              onChanged: (value) {
                                setState(() {
                                  selectedOption = value;
                                  idLeaveReason = state.dataLeaveReason['type_leave'][index]["id"];
                                });
                              },
                            );
                          },
                        );
                      }
                      else if(state is LeaveReasonFailed){
                        print("error cuy");
                        print("error cuy => "+state.msg);

                        return Container(color: Colors.blue);
                      } else if(state is LeaveReasonError){
                        print("error cuy");
                        print("error cuy => "+state.msg);

                        return Container(color: Colors.red);
                      }
                      return Container();
                    },
                  )
              ),
              Container(margin: EdgeInsets.all(16),child: RichText(
                text: TextSpan(
                  style: new TextStyle(
                      height: 1.1
                  ),
                  children: <TextSpan>[
                    new TextSpan(
                      text: 'Start Of Leave',
                      style: TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                    new TextSpan(
                      text: '*',
                      style: new TextStyle(fontSize: 15,color: color_secondary,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                textAlign: TextAlign.center,
              )),
              // Container(margin: EdgeInsets.all(16),child: Text("Start Of Leave",style: TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),)),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                child: CustomDatePickerForm(
                  controller: fromDateController,
                  onTap: (){
                    showDatePicker(
                      context: context,
                      initialDate: selectedFromYears == null ? DateTime.now():DateTime(selectedFromYears,selectedFromMonth,selectedFromdDay),
                      firstDate: DateTime(initYears,initMonth,initDay),
                      lastDate: selectedToYears == null ? DateTime(2099):DateTime(selectedToYears == null ? initYears:selectedToYears,selectedToMonth == null ?initMonth:selectedToMonth,selectedToDay == null ? initDay:selectedToDay),

                    ).then((date) {  //tambahkan setState dan panggil variabel _dateTime.
                      setState(() {
                        if(date == null){
                          print("date null");
                        } else {
                          print("date not null");
                          _dateTime = date;
                          fromDateController.text = _formatDate(date);
                          confirmSelectedFromDate = _formatDate(date);
                          selectedFromYears = _dateTime.year;
                          selectedFromMonth = _dateTime.month;
                          selectedFromdDay = _dateTime.day;
                        }
                      });
                    });
                  },
                )
              ),
              Container(margin: EdgeInsets.all(16),child: RichText(
                text: TextSpan(
                  style: new TextStyle(
                      height: 1.1
                  ),
                  children: <TextSpan>[
                    new TextSpan(
                      text: 'End Of Leave',
                      style: TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                    new TextSpan(
                      text: '*',
                      style: new TextStyle(fontSize: 15,color: color_secondary,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                textAlign: TextAlign.center,
              )),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                child: CustomDatePickerForm(
                  controller: toDateController,
                  onTap: (){
                    showDatePicker(
                      context: context,
                      initialDate: selectedToYears == null ? selectedFromYears == null ?DateTime.now():DateTime(selectedFromYears == null ? initYears:selectedFromYears,selectedFromMonth == null ?initMonth:selectedFromMonth,selectedFromdDay == null ? initDay:selectedFromdDay): DateTime(selectedToYears == null ? initYears:selectedToYears,selectedToMonth == null ?initMonth:selectedToMonth,selectedToDay == null ? initDay:selectedToDay),
                      firstDate: DateTime(selectedFromYears == null ? initYears:selectedFromYears,selectedFromMonth == null ?initMonth:selectedFromMonth,selectedFromdDay == null ? initDay:selectedFromdDay),
                      lastDate: DateTime(2099),

                    ).then((date) {  //tambahkan setState dan panggil variabel _dateTime.
                      setState(() {
                        if(date == null){
                          print("date null");
                        } else {
                          print("date not null");
                          _dateTime = date;
                          toDateController.text = _formatDate(date);
                          confirmSelectedToDate = _formatDate(date);
                          selectedToYears = _dateTime.year;
                          selectedToMonth = _dateTime.month;
                          selectedToDay = _dateTime.day;
                        }
                      });
                    });
                  },
                )
              ),
              Container(margin: EdgeInsets.all(16),child: Text("Leave Reason",style: TextStyle(fontSize: 15,color: color_black,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),)),

            Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 3,
                      offset: Offset(0, 0),
                    ),
                  ],
                ),
              margin: EdgeInsets.only(left: 16,right: 16,bottom: 32),
                padding: EdgeInsets.all(16),
                child: TextField(
                  controller: te_leave_reason,
                  style: TextStyle(
                      fontFamily: baseUrlFontsPoppinsRegular),
                  maxLines: 5,
                  maxLength: 150,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Input your leave reason"
                  ),
                  onChanged: (text){
                    setState(() {
                      leaveReason = text;
                    });
                  },
                  //onSubmitted: _getNotes(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
