import 'package:absen_online/bloc/login_cubit/login_cubit.dart';
import 'package:absen_online/constant/assets_constant.dart';
import 'package:absen_online/constant/color_constant.dart';
import 'package:absen_online/main.dart';
import 'package:absen_online/ui/screen/home_navigation.dart';
import 'package:absen_online/ui/support/flushbar/flushbar_notification.dart';
import 'package:absen_online/ui/widget/button/custom_button_primary.dart';
import 'package:absen_online/ui/widget/button/custom_text_form.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:permission_handler/permission_handler.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _TeUsername = TextEditingController();
  final TextEditingController _TePassword = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isButtonEnable = false,isButtonLoading = false,showPassword  = true;

  final LocalAuthentication auth = LocalAuthentication();
  LocalAuthentication localAuth = LocalAuthentication();
  bool _canCheckBiometrics = false;
  List<BiometricType> _availableBiometrics;
  bool userBiometricAvailable = false;
  String _authorized = 'Not Authorized';
  bool _isAuthenticating = false;

  void _handleLogin(BuildContext context) async{
    String token_fcm ="";

    /// Get token firebase
    // try{
    //   await firebaseMessaging.getToken().then((String token) {
    //     token_fcm = token;
    //   });
    // }catch(e){
    //   token_fcm = "";
    // }
    //
    // print("fcm token nya => "+token_fcm);
    context.read<LoginCubit>().login(_TeUsername.text, _TePassword.text,token_fcm);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    requestCameraPermission();
    firstAction();
  }

  Future<void> requestCameraPermission() async {
    // Request camera permission
    var status = await Permission.camera.request();

    // Check if permission is granted
    if (status.isGranted) {
      print("Camera permission granted");
      // Camera permission is granted
      // You can now use the camera
    } else if (status.isDenied) {
      print("Camera permission denied");

      // Camera permission is denied
      // You can show a dialog to inform the user and ask them to grant the permission manually
    } else if (status.isPermanentlyDenied) {
      print("Camera permission permanently denied");

      // Camera permission is permanently denied
      // You can show a dialog to inform the user and redirect them to the app settings
    }
  }

  Future<void> firstAction() async {
    await _checkBiometrics();
  }

  Future<void> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await localAuth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;
    _getAvailableBiometrics();
    // List<BiometricType> availableBiometrics =
    // await auth.getAvailableBiometrics();
    // print("AVAIL BIO RESULT => "+availableBiometrics.first.toString());
    // print("AVAIL BIO RESULT => "+availableBiometrics.last.toString());
    // print("AVAIL BIO RESULT => "+availableBiometrics.single.toString());
    setState(() {
      _canCheckBiometrics = canCheckBiometrics;
    });
  }

  Future<void> _getAvailableBiometrics() async {
    List<BiometricType> availableBiometrics;
    try {
      availableBiometrics = await localAuth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

    setState(() {
      _availableBiometrics = availableBiometrics;
      print(_availableBiometrics);
    });
  }



  Future<void> _authenticate() async {
    bool authenticated = false;
    try {
      setState(() {
        _isAuthenticating = true;
        _authorized = 'Authenticating';
      });

      const ANDROID_AUTH_MESSAGE = AndroidAuthMessages(
        signInTitle: 'Biometric Authentication',
        // fingerprintHint: 'Hint',
        // cancelButton: 'Cancel Button',
        // fingerprintNotRecognized: 'Nor Recognized',
        // fingerprintRequiredTitle: 'Required Title',
        // fingerprintSuccess: 'Success',
        // goToSettingsButton: 'go to setting',
        // goToSettingsDescription: 'go to setting description'
      );

      List<BiometricType> availableBiometrics =
      await auth.getAvailableBiometrics();

      // if (availableBiometrics.contains(BiometricType.face)) {
        // Face ID.
        authenticated = await localAuth.authenticateWithBiometrics(
          localizedReason: 'Please Authenticate',
          useErrorDialogs: true,
          stickyAuth: true,
          // androidAuthStrings: ANDROID_AUTH_MESSAGE,
        );

        print(authenticated);

        setState(() {
          _isAuthenticating = false;
          _authorized = 'Authenticating';
        });

        print("FINGER RESULT => "+authenticated.toString());
        authenticated ? print("SUCCESS FACE RECOGNITION") : null;
      // } else {
        ///do nothin
      // }


    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

    final String message = authenticated ? 'Authorized' : 'Not Authorized';
    setState(() {
      _authorized = message;
    });
  }

  void validateAndSave() {
    final FormState form = _formKey.currentState;
    if (form.validate()) {
      print('Form is valid');
    } else {
      print('Form is invalid');
    }
    Future.delayed(Duration(seconds: 2),(){
      setState(() {
        isButtonLoading = false;
      });
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
    });
  }

  void _listenerAuth(BuildContext context, LoginState state) {
    if (state is LoginLoading) {
      print('Login Loading');
    }
    if (state is LoginFailed) {
      print('Login Failed');
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      // FlushbarNotif.failedBottom(context, "Username atau password salah");

    }
    if (state is LoginSucces) {
      // FlushbarNotif.successBottom(context, "Login Berhasil");
      print('Login Succezz');
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
      // Future.delayed(Duration(seconds: 3), () {
      //   Navigator.pushReplacement(context,
      //       new MaterialPageRoute(builder: (context) => HomeNavigation(0)));
      // });
    }
    if (state is LoginError) {
      print('state error');
      print('state error => '+state.msg);

      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Text("Login",style: TextStyle(
                  fontFamily: baseUrlFontsPoppinsSemiBold,
                  color: color_black,
                  fontSize: 20
                ),),
              ),
              Container(
                padding: EdgeInsets.all(16),
                child: CustomTextForm(
                  "Email", "Input email",
                  type: 1,
                  textEditingController: _TeUsername,
                  onChanged: (value) {
                    print(value);
                    if(value.isNotEmpty && _TePassword.text.isNotEmpty){
                      setState(() {
                        isButtonEnable = true;
                      });
                    }else {
                      setState(() {
                        isButtonEnable = false;
                      });
                    }
                },),
              ),
              Container(
                padding: EdgeInsets.all(16),
                child: CustomTextForm(
                  "Password", "Masukkan password",
                  type: 2,
                  showPassword: showPassword,
                  suffixIcon: GestureDetector(
                    onTap: () {
                      setState(() {
                        showPassword = !showPassword;
                      });
                    },
                    child: Icon(showPassword
                        ? Icons.visibility_off
                        : Icons.visibility, size: 16,color: Colors.red,),
                  ),
                  textEditingController: _TePassword,
                  onChanged: (value) {
                    print(value);
                    if(value.isNotEmpty && _TeUsername.text.isNotEmpty){
                      setState(() {
                        isButtonEnable = true;
                      });
                    } else {
                      setState(() {
                        isButtonEnable = false;
                      });
                    }
                  },),
              ),
              BlocConsumer<LoginCubit, LoginState>(
                builder: (context,state){
                  return Container(
                    padding: EdgeInsets.all(16),
                    child: CustomButtonPrimary(
                      color: color_primary,
                      title: "login".toUpperCase(),
                      textColor: Colors.white,
                      isEnable: isButtonEnable,
                      btnLoading: (state is LoginLoading) ? true : false,
                      onTap: (){
                        if(state is !LoginLoading){
                          setState(() {
                            isButtonLoading = true;
                          });
                          // validateAndSave();
                          _handleLogin(context);
                          // _checkBiometrics();
                          // _authenticate();
                        }
                      },
                      borderRadius: 50,
                    ),
                  );
                },
                listener: _listenerAuth
              ),
            ],
          ),
        ),
      ),
    );
  }
}
