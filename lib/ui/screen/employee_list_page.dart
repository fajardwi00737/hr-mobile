import 'package:absen_online/bloc/employee_bloc/employee_cubit.dart';
import 'package:absen_online/constant/assets_constant.dart';
import 'package:absen_online/constant/color_constant.dart';
import 'package:absen_online/ui/screen/auth/login_page.dart';
import 'package:absen_online/ui/support/flushbar/flushbar_notification.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EmployeeListPage extends StatefulWidget {
  const EmployeeListPage({Key key}) : super(key: key);

  @override
  State<EmployeeListPage> createState() => _EmployeeListPageState();
}

class _EmployeeListPageState extends State<EmployeeListPage> {

  firstTimeOpenThisPage()async{
    print("get data employee");
    context.read<EmployeeCubit>().getEmployee();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firstTimeOpenThisPage();
  }

  void listenerEmployee(BuildContext context, EmployeeState state) {
    if (state is EmployeeLoading) {
      print('absen Loading');
    }
    if (state is EmployeeFailed) {
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          "Token has expired and can no longer be refreshed",
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      Future.delayed(Duration(seconds: 2), () {
        logout(context);
      });
      // FlushbarNotif.failedBottom(context, "Username atau password salah");

    }
    if (state is EmployeeSuccess) {
      // FlushbarNotif.successBottom(context, "Login Berhasil");
      print('absen Succezz');
      // FlushbarNotification.flushbarTop(
      //     context,
      //     FlushbarPosition.BOTTOM,
      //     state.msg,
      //     Colors.white,
      //     color_warning,
      //     Icon(
      //       Icons.check_circle,
      //       color: Colors.white,
      //     ),
      //     false);

      // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
      // Future.delayed(Duration(seconds: 3), () {
      //   Navigator.pushReplacement(context,
      //       new MaterialPageRoute(builder: (context) => HomeNavigation(0)));
      // });
    }
    if (state is EmployeeError) {
      print('state error');
      print('state error => '+state.msg);
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          "Token has expired and can no longer be refreshed",
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      Future.delayed(Duration(seconds: 2), () {
        logout(context);
      });
    }
  }

  void logout(BuildContext context)async{
    await GeneralSharedPreferences.remove("user_id");
    await GeneralSharedPreferences.remove("token_login");
    await GeneralSharedPreferences.remove("is_login");
    await GeneralSharedPreferences.remove("user_name");
    await GeneralSharedPreferences.remove("user_mail");
    await GeneralSharedPreferences.remove("user_phone");
    await GeneralSharedPreferences.remove("user_address");
    await GeneralSharedPreferences.remove("fcm_token");
    await GeneralSharedPreferences.remove("user_divisi");
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => LoginPage()), (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Employee List",style: TextStyle(color: Colors.black),),
        centerTitle: true,
        leading: IconButton(
          color: Colors.black,
          icon: Icon(Icons.arrow_back_rounded,color: Colors.black,),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        child: BlocConsumer<EmployeeCubit,EmployeeState>(
            listener: listenerEmployee,
        builder: (context, state){
          return BlocBuilder<EmployeeCubit,EmployeeState>(
            builder: (index,state){
              if(state is EmployeeLoading){
                return Center(
                  child: Container(
                    height: 25,
                    width: 25,
                    child: CircularProgressIndicator(
                      // valueColor: Colors.white,
                      backgroundColor: Colors.blue,
                    ),
                  ),
                );
              }
              else if(state is EmployeeSuccess){
                return ListView.builder(
                    shrinkWrap: true,
                    itemCount: state.dataEmployee["employee"].length,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.only(left: 16,right: 16,top: 16),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 10),
                              height: 50,
                              width: 50,
                              decoration: BoxDecoration(
                                  color: Colors.grey[300],
                                  shape: BoxShape.circle
                              ),
                              child: Center(
                                child: Icon(Icons.person_rounded,color: Colors.grey[600],),
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("${state.dataEmployee["employee"][index]["first_name"]} ${state.dataEmployee["employee"][index]["last_name"]}",style: TextStyle(color: color_black,fontSize:17,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                                Text(state.dataEmployee["employee"][index]["contact_no"],style: TextStyle(color: color_grey,fontSize:14,fontFamily: baseUrlFontsPoppinsRegular))
                              ],
                            ),
                          ],
                        ),
                      );
                    }
                );
              }
              else if(state is EmployeeFailed){
                print("error cuy");
                print("error cuy => "+state.msg);

                return Container(color: Colors.white);
              }else if(state is EmployeeError){
                print("error cuy");
                print("error cuy => "+state.msg);

                return Container(color: Colors.white);
              }
              return Container();
            },
          );
        }

        )
      ),
    );
  }
}
