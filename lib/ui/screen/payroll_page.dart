import 'package:absen_online/bloc/payroll_cubit/payroll_cubit.dart';
import 'package:absen_online/constant/assets_constant.dart';
import 'package:absen_online/constant/color_constant.dart';
import 'package:absen_online/ui/screen/auth/login_page.dart';
import 'package:absen_online/ui/support/flushbar/flushbar_notification.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';


class PayrollPage extends StatefulWidget {
  const PayrollPage({Key key}) : super(key: key);

  @override
  State<PayrollPage> createState() => _PayrollPageState();
}

class _PayrollPageState extends State<PayrollPage> {

  firstTimeOpenThisPage()async{
    print("get data payroll");
    context.read<PayrollCubit>().getPayroll("February","25/02/2024");
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firstTimeOpenThisPage();
  }

  String formatCurrency(int amount) {
    final formatter = NumberFormat.currency(locale: 'id_ID', symbol: 'Rp. ');
    return formatter.format(amount);
  }

  int _selectedTileIndex = -1;

  void _handleTileTap(int index) {
    setState(() {
      if (_selectedTileIndex == index) {
        _selectedTileIndex = -1; // Collapse the selected tile if it's tapped again
      } else {
        _selectedTileIndex = index; // Expand the selected tile
      }
    });
  }

  void listenerEmployee(BuildContext context, PayrollState state) {
    if (state is PayrollLoading) {
      print('absen Loading');
    }
    if (state is PayrollFailed) {
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          "Token has expired and can no longer be refreshed",
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      Future.delayed(Duration(seconds: 2), () {
        logout(context);
      });
      // FlushbarNotif.failedBottom(context, "Username atau password salah");

    }
    if (state is PayrollSuccess) {
      // FlushbarNotif.successBottom(context, "Login Berhasil");
      print('absen Succezz');
      // FlushbarNotification.flushbarTop(
      //     context,
      //     FlushbarPosition.BOTTOM,
      //     state.msg,
      //     Colors.white,
      //     color_warning,
      //     Icon(
      //       Icons.check_circle,
      //       color: Colors.white,
      //     ),
      //     false);

      // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
      // Future.delayed(Duration(seconds: 3), () {
      //   Navigator.pushReplacement(context,
      //       new MaterialPageRoute(builder: (context) => HomeNavigation(0)));
      // });
    }
    if (state is PayrollError) {
      print('state error');
      print('state error => '+state.msg);
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          "Token has expired and can no longer be refreshed",
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      Future.delayed(Duration(seconds: 2), () {
        logout(context);
      });
    }
  }

  void logout(BuildContext context)async{
    await GeneralSharedPreferences.remove("user_id");
    await GeneralSharedPreferences.remove("token_login");
    await GeneralSharedPreferences.remove("is_login");
    await GeneralSharedPreferences.remove("user_name");
    await GeneralSharedPreferences.remove("user_mail");
    await GeneralSharedPreferences.remove("user_phone");
    await GeneralSharedPreferences.remove("user_address");
    await GeneralSharedPreferences.remove("fcm_token");
    await GeneralSharedPreferences.remove("user_divisi");
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => LoginPage()), (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Payroll",
          style: TextStyle(
            color: Colors.black
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          color: Colors.black,
          icon: Icon(Icons.arrow_back_rounded,color: Colors.black,),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
      ),
      body: BlocConsumer<PayrollCubit,PayrollState>(
        listener: listenerEmployee,
        builder: (context, state){
          return Container(
              child: BlocBuilder<PayrollCubit,PayrollState>(
                builder: (context,state){
                  if(state is PayrollLoading){
                    return Center(
                      child: Container(
                        height: 25,
                        width: 25,
                        child: CircularProgressIndicator(
                          // valueColor: Colors.white,
                          backgroundColor: Colors.blue,
                        ),
                      ),
                    );
                  }
                  else if(state is PayrollSuccess){
                    return SingleChildScrollView(
                      child: _mainBody(state)
                    );
                  }else if (state is PayrollFailed){
                    print("error cuy");
                    print("error cuy => "+state.msg);

                    return Container(color: Colors.white);
                  } else if (state is PayrollError){
                    print("error cuy");
                    print("error cuy => "+state.msg);

                    return Container(color: Colors.white);
                  }
                  return Container();
                },
              )
          );
        }
      ),
    );
  }



  _mainBody(PayrollSuccess state){
    return state.dataPayroll["history"].length == 0 ? Container(margin: EdgeInsets.all(16),child: Center(child: Text("Data Not Found",style: TextStyle(color: color_black,fontSize:15,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)))):ListView.builder(
        itemCount: state.dataPayroll["history"].length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
      return Container(
        margin: EdgeInsets.only(left: 16,right: 16,top: 16,bottom: index ==state.dataPayroll["history"].length-1 ? 16:0),
        child: Card(
          child: ExpansionTile(

            title: Text(state.dataPayroll["history"][index]["month_year"],style: TextStyle(color: color_black,fontSize:15,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold),),
            children: [_mainContent(state,index)],
          ),
        ),
      );
    }
    );
  }

  _mainContent(PayrollSuccess state,int index){
    return Container(
      // margin: EdgeInsets.all(16),
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,

              children: [
                Text("Basic Salary",style: TextStyle(color: color_black,fontSize:15,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                SizedBox(height: 6,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,

                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Basic Salary : ",style: TextStyle(color: color_grey,fontSize:12,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                    Text(formatCurrency(int.parse(state.dataPayroll["history"][index]["basic_salary"].split('.')[0])),style: TextStyle(color: color_black,fontSize:17,fontFamily: baseUrlFontsPoppinsRegular,fontWeight: FontWeight.bold))
                  ],
                ),
              ],
            ),
          ),
          state.dataPayroll["history"][index]['allowances'].length == 0 ? Container():Container(margin: EdgeInsets.symmetric(vertical: 5),child: Divider(thickness: 0.5,color: Color(0xFFBEBEBE),)),
          state.dataPayroll["history"][index]['allowances'].length == 0 ? Container():Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,

              children: [
                Text("Allowance",style: TextStyle(color: color_black,fontSize:15,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                SizedBox(height: 6,),
                ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: state.dataPayroll["history"][index]["allowances"].length,
                    itemBuilder: (context, i) {
                      return Row(
                        crossAxisAlignment: CrossAxisAlignment.center,

                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("${state.dataPayroll["history"][index]["allowances"][i]["allowance_title"]} : ",style: TextStyle(color: color_grey,fontSize:12,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                          Text(formatCurrency(int.parse(state.dataPayroll["history"][index]["allowances"][i]["allowance_amount"])),style: TextStyle(color: color_black,fontSize:17,fontFamily: baseUrlFontsPoppinsRegular,fontWeight: FontWeight.bold))
                        ],
                      );
                    }
                ),
                SizedBox(height: 10,),
                // Text("New Allowance --",style: TextStyle(color: color_black,fontSize:15,fontStyle: FontStyle.italic,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                // SizedBox(height: 6,),
                // ListView.builder(
                //     physics: NeverScrollableScrollPhysics(),
                //     shrinkWrap: true,
                //     itemCount: state.dataPayroll['employee']['result_allowance_component'].length,
                //     itemBuilder: (context, index) {
                //       return Row(
                //         crossAxisAlignment: CrossAxisAlignment.center,
                //
                //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //         children: [
                //           Text("${state.dataPayroll['employee']['result_allowance_component'][index]["title"]} : ",style: TextStyle(color: color_grey,fontSize:12,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                //           Text(formatCurrency(int.parse(state.dataPayroll['employee']['result_allowance_component'][index]["amount"])),style: TextStyle(color: color_black,fontSize:17,fontFamily: baseUrlFontsPoppinsRegular,fontWeight: FontWeight.bold))
                //         ],
                //       );
                //     }
                // ),
              ],
            ),
          ),
          state.dataPayroll["history"][index]['overtimes'].length == 0 ? Container():Container(margin: EdgeInsets.symmetric(vertical: 5),child: Divider(thickness: 0.5,color: Color(0xFFBEBEBE),)),
          state.dataPayroll["history"][index]['overtimes'].length == 0 ? Container():Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,

              children: [
                Text("Overtimes",style: TextStyle(color: color_black,fontSize:15,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                SizedBox(height: 6,),
                ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: state.dataPayroll["history"][index]["overtimes"].length,
                    itemBuilder: (context, i) {
                      return Row(
                        crossAxisAlignment: CrossAxisAlignment.center,

                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("${formatCurrency(int.parse(state.dataPayroll["history"][index]["overtimes"][i]["amount"]))} x ${state.dataPayroll["history"][index]["overtimes"][i]["hour"] == 0 ?state.dataPayroll["history"][index]["overtimes"][i]["day"]:state.dataPayroll["history"][index]["overtimes"][i]["hour"]} ${state.dataPayroll["history"][index]["overtimes"][i]["overtime_type"]} : ",style: TextStyle(color: color_grey,fontSize:12,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                          Text(overtimeCal(int.parse(state.dataPayroll["history"][index]["overtimes"][i]["amount"]),state.dataPayroll["history"][index]["overtimes"][i]["hour"] == 0 ?state.dataPayroll["history"][index]["overtimes"][i]["day"]:state.dataPayroll["history"][index]["overtimes"][i]["hour"]),style: TextStyle(color: color_black,fontSize:17,fontFamily: baseUrlFontsPoppinsRegular,fontWeight: FontWeight.bold))
                        ],
                      );
                    }
                ),
                SizedBox(height: 10,),
                // Text("New Allowance --",style: TextStyle(color: color_black,fontSize:15,fontStyle: FontStyle.italic,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                // SizedBox(height: 6,),
                // ListView.builder(
                //     physics: NeverScrollableScrollPhysics(),
                //     shrinkWrap: true,
                //     itemCount: state.dataPayroll['employee']['result_allowance_component'].length,
                //     itemBuilder: (context, index) {
                //       return Row(
                //         crossAxisAlignment: CrossAxisAlignment.center,
                //
                //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //         children: [
                //           Text("${state.dataPayroll['employee']['result_allowance_component'][index]["title"]} : ",style: TextStyle(color: color_grey,fontSize:12,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                //           Text(formatCurrency(int.parse(state.dataPayroll['employee']['result_allowance_component'][index]["amount"])),style: TextStyle(color: color_black,fontSize:17,fontFamily: baseUrlFontsPoppinsRegular,fontWeight: FontWeight.bold))
                //         ],
                //       );
                //     }
                // ),
              ],
            ),
          ),
          state.dataPayroll["history"][index]['reimbursements'].length == 0 ? Container():Container(margin: EdgeInsets.symmetric(vertical: 5),child: Divider(thickness: 0.5,color: Color(0xFFBEBEBE),)),
          state.dataPayroll["history"][index]['reimbursements'].length == 0 ? Container():Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,

              children: [
                Text("Reimbursement",style: TextStyle(color: color_black,fontSize:15,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                SizedBox(height: 6,),
                ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: state.dataPayroll["history"][index]['reimbursements'].length,
                    itemBuilder: (context, k) {
                      return Row(
                        crossAxisAlignment: CrossAxisAlignment.center,

                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(state.dataPayroll["history"][index]['reimbursements'][k]["description"],style: TextStyle(color: color_grey,fontSize:12,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
                          Text(formatCurrency(int.parse(state.dataPayroll["history"][index]['reimbursements'][k]["nominal"])),style: TextStyle(color: color_black,fontSize:17,fontFamily: baseUrlFontsPoppinsRegular,fontWeight: FontWeight.bold))
                        ],
                      );
                    }
                ),
              ],
            ),
          ),
          // Container(margin: EdgeInsets.symmetric(vertical: 5),child: Divider(thickness: 0.5,color: Color(0xFFBEBEBE),)),
          // Container(
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //
          //     children: [
          //       Text("Deduction",style: TextStyle(color: color_black,fontSize:15,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
          //       SizedBox(height: 6,),
          //       ListView.builder(
          //           physics: NeverScrollableScrollPhysics(),
          //           shrinkWrap: true,
          //           itemCount: state.dataPayroll['employee']['result_deduction'].length,
          //           itemBuilder: (context, index) {
          //             return Row(
          //               crossAxisAlignment: CrossAxisAlignment.center,
          //
          //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //               children: [
          //                 Text("PPH : ",style: TextStyle(color: color_grey,fontSize:12,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
          //                 Text(formatCurrency(int.parse(state.dataPayroll['employee']['result_deduction'][index]["pph_paid"])),style: TextStyle(color: color_black,fontSize:17,fontFamily: baseUrlFontsPoppinsRegular,fontWeight: FontWeight.bold))
          //               ],
          //             );
          //           }
          //       ),
          //       Text("New Deduction --",style: TextStyle(color: color_black,fontSize:15,fontStyle: FontStyle.italic,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
          //       SizedBox(height: 6,),
          //       ListView.builder(
          //           physics: NeverScrollableScrollPhysics(),
          //           shrinkWrap: true,
          //           itemCount: state.dataPayroll['employee']['result_deduction_component'].length,
          //           itemBuilder: (context, index) {
          //             return Row(
          //               crossAxisAlignment: CrossAxisAlignment.center,
          //
          //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //               children: [
          //                 Text("PPH : ",style: TextStyle(color: color_grey,fontSize:12,fontFamily: baseUrlFontsPoppinsSemiBold,fontWeight: FontWeight.bold)),
          //                 Text(formatCurrency(int.parse(state.dataPayroll['employee']['result_deduction_component'][index]["pph_paid"])),style: TextStyle(color: color_black,fontSize:17,fontFamily: baseUrlFontsPoppinsRegular,fontWeight: FontWeight.bold))
          //               ],
          //             );
          //           }
          //       ),
          //     ],
          //   ),
          // ),
          Container(margin: EdgeInsets.symmetric(vertical: 5),child: Divider(thickness: 1.5,color: Color(0xFFBEBEBE),)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,

            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Take Home Pay : ",overflow: TextOverflow.ellipsis,style: TextStyle(color: color_black,fontSize: 15,fontFamily: baseUrlFontsPoppinsSemiBold)),
              Text(formatCurrency(int.parse(state.dataPayroll["history"][index]['take_home_pay'].split('.')[0])),style: TextStyle(color: color_black,fontSize:17,fontFamily: baseUrlFontsPoppinsRegular,fontWeight: FontWeight.bold))
            ],
          ),
        ],
      ),
    );
  }

  String overtimeCal(int amount, int duration){
    int total = amount * duration;
    String totalString = formatCurrency(total);
    return totalString;
  }

  convertAmount(var amount){
    String originalString = amount.toString();
    String result = originalString.split('.')[0];
    return result;
  }
}
