import 'dart:async';
import 'dart:io';

import 'package:absen_online/bloc/absen_cubit/absen_cubit.dart';
import 'package:absen_online/bloc/verify_face_cubit/verify_face_cubit.dart';
import 'package:absen_online/constant/assets_constant.dart';
import 'package:absen_online/constant/color_constant.dart';
import 'package:absen_online/ui/screen/camera_stream_page.dart';
import 'package:absen_online/ui/screen/home_navigation.dart';
import 'package:absen_online/ui/support/flushbar/flushbar_notification.dart';
import 'package:absen_online/ui/widget/button/custom_button_confirm.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geocoder/model.dart';
import 'package:geocoding/geocoding.dart' as Geo;
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:local_auth/local_auth.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:camera/camera.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

import 'dart:async';
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:image/image.dart' as img;
import 'package:http/http.dart' as http;

class AbsensiPage extends StatefulWidget {
  @override
  _AbsensiPageState createState() => _AbsensiPageState();
}

class _AbsensiPageState extends State<AbsensiPage> {
  Position _position;
  Future _mapFuture;
  Completer<GoogleMapController> mapController = Completer();
  Map<MarkerId, Marker> _markersSBM = <MarkerId, Marker>{};
  String _timeString,_timeStringConfirm,userAddress = "",_dateString,_dateStringConfirm;
  Timer _timer;
  int absenType = 1;
  bool isvalidating = false;

  //camera
  CameraController _controller;
  bool _isInitialized = false;

  final LocalAuthentication auth = LocalAuthentication();
  LocalAuthentication localAuth = LocalAuthentication();
  bool _canCheckBiometrics = false;
  List<BiometricType> _availableBiometrics;
  bool userBiometricAvailable = false;
  String _authorized = 'Not Authorized';
  bool _isAuthenticating = false;

  XFile imageFile;

  List<CameraDescription> cameras;
  CameraController controller;
  Timer _timers;

  void startImageCapture() {
    setState(() {
      isvalidating = true;
    });
    // _timers = Timer.periodic(Duration(milliseconds: 500), (Timer timer) {
      captureAndSaveImage();
    // });
  }

  void stopImageCapture() {
    _timers.cancel();
  }

  Future<void> initCamera() async {
    cameras = await availableCameras();
    for (CameraDescription camera in cameras) {
      if (camera.lensDirection == CameraLensDirection.front) {
        controller = CameraController(camera, ResolutionPreset.medium);
        await controller.initialize();
        setState(() {});
        break;
      }
    }
  }

  StreamController<Image> imageStreamController = StreamController<Image>();

  void startStreaming() {
    controller.startImageStream((CameraImage image) {
      Image convertedImage = convertImage(image);
      imageStreamController.add(convertedImage);
    });
  }

  void stopStreaming() {
    controller.stopImageStream();
    imageStreamController.close();
  }

  Future<void> captureAndSaveImage() async {
    XFile image = await controller.takePicture();

    if (image != null) {
      final Directory appDirectory = await getApplicationDocumentsDirectory();
      final String imagePath = '${appDirectory.path}/image.jpg';

      final File imageFile = File(imagePath);
      await imageFile.writeAsBytes(await image.readAsBytes());

      context.read<VerifyFaceCubit>().verifyFace(imageFile);
      // await uploadImageFile(imageFile);
    }
  }

  Image convertImage(CameraImage cameraImage) {
    img.Image convertedImage = img.Image(
      cameraImage.height,
      cameraImage.width,
    );
    img.Image rotatedImage = img.copyRotate(convertedImage, 90);
    return Image.memory(img.encodeJpg(rotatedImage));
  }

  Future<void> uploadImageFile(File file) async {
    final url = Uri.parse('YOUR_API_ENDPOINT');
    final request = http.MultipartRequest('POST', url);
    request.files.add(await http.MultipartFile.fromPath('image', file.path));

    try {
      final response = await request.send();
      if (response.statusCode == 200) {
        print('Image uploaded successfully');
      } else {
        print('Failed to upload image. Status code: ${response.statusCode}');
      }
    } catch (e) {
      print('Error uploading image: $e');
    }
  }

  TextEditingController notesController = TextEditingController();
  List<DropdownMenuItem<String>> get dropdownItems{
    List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(child: Text("Clock In"),value: "present"),
      DropdownMenuItem(child: Text("Clock Out"),value: "out_present"),
    ];
    return menuItems;
  }
  String selectedValue = "present";
  @override
  void initState() {
    initCamera();
    fistOpenPage();
    _mapFuture = _getCurrentLocationInit();
    _timeString = formatTime(DateTime.now());
    _dateString = _formatDate(DateTime.now());
    _timer = Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    requestCameraPermission();
    firstAction();
    // initCamera().then((_) {
    //   setState(() {
    //     _isInitialized = true;
    //   });
    // });
    print("print location");
    print(getAddressFromLatLng());
    // TODO: implement initState
    super.initState();
  }



  File _imagePicked;
  Future<void> captureImage() async {
    print('verifying face');
    final image = await ImagePicker().getImage(source: ImageSource.camera);
    setState(() {
      if (image != null) {
        print('Getting data');
        _imagePicked = File(image.path);
      } else {
        print('No image selected.');
      }
    });
    File compressedFile = await compressImage(_imagePicked, 70);
    print('Store data');
    context.read<VerifyFaceCubit>().verifyFace(compressedFile);
    // showMyAlertDialog(context);
  }

  Future<File> compressImage(File file, int quality) async {
    List<int> bytes = await file.readAsBytes();
    var result = await FlutterImageCompress.compressWithList(
      bytes,
      quality: quality,
    );

    // Write compressed image to a new file
    File compressedFile = File('${file.path}.compressed.jpg');
    await compressedFile.writeAsBytes(result);
    return compressedFile;
  }

  String formatTime(DateTime time) {
    final formatter = DateFormat.jm();
    return formatter.format(time);
  }

  Future<void> requestCameraPermission() async {
    // Request camera permission
    var status = await Permission.camera.request();
    // PermissionStatus stat = await Permission.biometrics.request();
    var loc = await Permission.locationWhenInUse.request();

    if (loc.isGranted) {
      print("Camera permission granted");
      // Camera permission is granted
      // You can now use the camera
    }
    // Check if permission is granted
    if (status.isGranted) {
      print("Camera permission granted");
      // Camera permission is granted
      // You can now use the camera
    } else if (status.isDenied) {
      print("Camera permission denied");

      // Camera permission is denied
      // You can show a dialog to inform the user and ask them to grant the permission manually
    } else if (status.isPermanentlyDenied) {
      print("Camera permission permanently denied");

      // Camera permission is permanently denied
      // You can show a dialog to inform the user and redirect them to the app settings
    }
  }

  Future<void> firstAction() async {
    await _checkBiometrics();
  }

  Future<void> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;
    _getAvailableBiometrics();
    // List<BiometricType> availableBiometrics =
    // await auth.getAvailableBiometrics();
    // print("AVAIL BIO RESULT => "+availableBiometrics.first.toString());
    // print("AVAIL BIO RESULT => "+availableBiometrics.last.toString());
    // print("AVAIL BIO RESULT => "+availableBiometrics.single.toString());
    setState(() {
      _canCheckBiometrics = canCheckBiometrics;
    });
  }

  Future<void> _getAvailableBiometrics() async {
    List<BiometricType> availableBiometrics;
    try {
      availableBiometrics = await auth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

    setState(() {
      _availableBiometrics = availableBiometrics;
      print(_availableBiometrics);
    });
  }

  Future<XFile> takePicture() async {
    final CameraController cameraController = _controller;
    if (cameraController == null || !cameraController.value.isInitialized) {
      print('Error: select a camera first.');
      return null;
    }

    if (cameraController.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      final XFile file = await cameraController.takePicture();
      return file;
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
  }

  void _showCameraException(CameraException e) {
    print('Error: ${e.code}\n${e.description}');
  }

  void onTakePictureButtonPressed() {
    takePicture().then((XFile file) {
      if (mounted) {
        setState(() {
          imageFile = file;
        });
        if (file != null) {
          print('Picture saved to ${file.path}');
        }
      }
    });
  }


  Future<void> _authenticate() async {
    print("set input presensi");
    bool authenticated = false;
    try {
      setState(() {
        _isAuthenticating = true;
        _authorized = 'Authenticating';
      });

      const ANDROID_AUTH_MESSAGE = AndroidAuthMessages(
        signInTitle: 'Biometric Authentication',
        // fingerprintHint: 'Hint',
        // cancelButton: 'Cancel Button',
        // fingerprintNotRecognized: 'Nor Recognized',
        // fingerprintRequiredTitle: 'Required Title',
        // fingerprintSuccess: 'Success',
        // goToSettingsButton: 'go to setting',
        // goToSettingsDescription: 'go to setting description'
      );

      List<BiometricType> availableBiometrics =
      await auth.getAvailableBiometrics();

      // if (availableBiometrics.contains(BiometricType.face)) {
        // Face ID.
        authenticated = await auth.authenticateWithBiometrics(
          localizedReason: 'Scan your biometric to authenticate',
          useErrorDialogs: true,
          stickyAuth: true,
          androidAuthStrings: ANDROID_AUTH_MESSAGE,
        );

        print(authenticated);

        setState(() {
          _isAuthenticating = false;
          _authorized = 'Authenticating';
        });

        print("FINGER RESULT => "+authenticated.toString());
        authenticated ? print("SUCCESS FACE RECOGNITION") : null;
      // } else {
      //   ///do nothin
      // }
      if(authenticated){
        actionConfirmAttendance(context);
      } else {
        FlushbarNotification.flushbarTop(
            context,
            FlushbarPosition.BOTTOM,
            "Fingerprint not authenticated",
            Colors.white,
            color_warning,
            Icon(
              Icons.check_circle,
              color: Colors.white,
            ),
            false);
      }


    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;

    final String message = authenticated ? 'Authorized' : 'Not Authorized';
    setState(() {
      _authorized = message;
    });
  }

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = formatTime(now);
    setState(() {
      _timeString = formattedDateTime;
    });
  }

  String _formatTime(DateTime dateTime) {
    return DateFormat('HH:mm:ss').format(dateTime);
  }

  String _formatDate(DateTime dateTime) {
    return DateFormat('dd MMMM yyyy').format(dateTime);
  }

  String _formatDate2(DateTime dateTime) {
    return DateFormat('dd-MM-yyyy').format(dateTime);
  }

  actionConfirmAttendance(BuildContext context)async{
    _timeStringConfirm = formatTime(DateTime.now());
    _dateStringConfirm = _formatDate2(DateTime.now());
    print("Lokasi => "+userAddress);
    print("Time => "+_timeStringConfirm);
    print("date => "+_dateStringConfirm);
    print("Type => "+absenType.toString());
    print("Keterangan => "+notesController.text.toString());
    print("Type Absen => "+selectedValue);
    print("Type Absen => "+_position.latitude.toString());
    print("Type Absen => "+_position.longitude.toString());

    context.read<AbsenCubit>().actionAbsen(GeneralSharedPreferences.readInt("user_id").toString(), _dateStringConfirm, _timeStringConfirm, selectedValue, userAddress,_position.latitude,_position.longitude);
  }

  @override
  void dispose() {
    controller.dispose();
    _timer.cancel();
    notesController.dispose();
    super.dispose();
  }

  fistOpenPage()async{
    Position res = await Geolocator().getCurrentPosition();

    setState(() {
      _position = res;
    });
  }

  Future getAddressFromLatLng() async {
    Position res = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    try {
      final coordinates = new Coordinates(
          res.latitude, res.longitude);
      var addresses = await Geocoder.google('AIzaSyD0n7NttGgYsWa-Rzir67FVFOe7IrEKK8M').findAddressesFromCoordinates(
          coordinates);
      var first = addresses.first;
      print("tes loc => ");
      print(' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');
    } catch (e) {
      print('Error fetching address: $e');

    }
  }

  Future _getCurrentLocationInit() async {
    //await _progressDialog.show();
    print("test get location");
    Position res = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.best);


    // try {
    //   List<Geo.Placemark> placemark = await Geo.placemarkFromCoordinates(
    //       res.latitude, res.longitude,localeIdentifier: "en");
    //   // for (int i = 0; i < placemark.length; i++) {
    //   //   print("city $i => " + placemark[i].toString());
    //   // }
    //   // print('hahahahah == ' + placemark[placemark.length > 1 ? 1 : 0]["Street"]);
    //   print('hahahahah == ' + placemark.toString());
    //   setState(() {
    //     // userAddress = placemark[placemark.length > 1 ? 1 : 0].street;
    //     userAddress = placemark[0].street;
    //   });
    // } catch (err) {
    //   print("error get location");
    //   print(err);
    // }

    try {
      final coordinates = new Coordinates(
          res.latitude, res.longitude);
      var addresses = await Geocoder.google('AIzaSyD0n7NttGgYsWa-Rzir67FVFOe7IrEKK8M').findAddressesFromCoordinates(
          coordinates);
      var first = addresses.first;
      print("tes loc => ");
      print(' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');
      setState(() {
        //     // userAddress = placemark[placemark.length > 1 ? 1 : 0].street;
            userAddress = first.featureName;
          });
    } catch (e) {
      print('Error fetching address: $e');

    }

    setState(() {
      _position = res;
      print('hahahaha ads== ' + _position.latitude.toString());
      print('hahahaha asd== ' + _position.longitude.toString());
      print('hahahaha asd== ' + res.toString());
      print('hahahaha asd== ' + userAddress);
    });

    MarkerId markerId = MarkerId("1");
    LatLng position = LatLng(_position.latitude, _position.longitude);
    Marker marker = Marker(
      markerId: markerId,
      position: position,
      draggable: false,
    );
    setState(() {
      _markersSBM[markerId] = marker;
    });
    //await updateCameraLocation();

    return _position;
  }
  void _onMapCreated(GoogleMapController controller)async {
    // await rootBundle
    //     .loadString('assets/maps/styleMapsAll.json')
    //     .then((String mapStyle) {
    //   controller.setMapStyle(mapStyle);
    // });
    mapController.complete(controller);

    MarkerId markerId = MarkerId("1");
    LatLng position = LatLng(_position.latitude, _position.longitude);
    Marker marker = Marker(
      markerId: markerId,
      position: position,
      draggable: false,
    );
    setState(() {
      _markersSBM[markerId] = marker;
    });
  }

  void _listenerVerifyFace(BuildContext context, VerifyFaceState state){
    if(state is VerifyFaceLoading){
      // showMyAlertDialog(context);
      setState(() {
        isvalidating = false;
      });
      print('absen Loading');
      stopStreaming();
      Navigator.pop(context);

    }if(state is VerifyFaceSuccess){
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          "Success Verify Face",
          Colors.white,
          color_success,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      setState(() {
        isvalidating = false;
      });
      // closeAlertDialog(context);
      Future.delayed(Duration(seconds: 3), () {
        actionConfirmAttendance(context);
        stopStreaming();
        Navigator.pop(context);
      });

    }if(state is VerifyFaceFailed){
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_warning,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      setState(() {
        isvalidating = false;
      });
      // closeAlertDialog(context);
      stopStreaming();
      Navigator.pop(context);

    }if(state is VerifyFaceError){
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_warning,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      setState(() {
        isvalidating = false;
      });
      // closeAlertDialog(context);
      stopStreaming();
      Navigator.pop(context);

    }
  }

  void _listenerAbsen(BuildContext context, AbsenState state) {
    if (state is AbsenLoading) {
      print('absen Loading');
    }
    if (state is AbsenFailed) {
      print('absen Failed');
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_warning,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      // FlushbarNotif.failedBottom(context, "Username atau password salah");

    }
    if (state is AbsenSucces) {
      // FlushbarNotif.successBottom(context, "Login Berhasil");
      print('absen Succezz');
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          state.msg,
          Colors.white,
          color_success,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
      // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
      Future.delayed(Duration(seconds: 2), () {
        // Navigator.pop(context,true);
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
      });
    }
    if (state is AbsenError) {
      print('state error');
      print('state error => '+state.msg);
      FlushbarNotification.flushbarTop(
          context,
          FlushbarPosition.BOTTOM,
          "Request failed! Please try again",
          Colors.white,
          color_failed,
          Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          false);
    }
  }

  void closeAlertDialog(BuildContext context) {
    Navigator.of(context).pop();
  }

  void showMyAlertDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Center(
            child: Container(
              height: 25,
              width: 25,
              child: CircularProgressIndicator(
                // valueColor: Colors.white,
                backgroundColor: Colors.blue,
              ),
            ),
          ),
          content: Center(child: Text('Verify Face...'))
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child:FutureBuilder(
          future: _mapFuture,
          builder:(context, snapshot) {
            if (snapshot.connectionState == ConnectionState.none &&
                snapshot.hasData == null) {
              // print('AYAAMMMMMM KAMPUSSSSSSSSSSS');
              //_progressDialog.dismiss();
              // _progressDialog.hide();
              return Container(color: Colors.red);
            } else if (snapshot.connectionState == ConnectionState.done) {
              //_progressDialog.dismiss();
              // _progressDialog.hide();
              // print('AYAAMMMMMM GOREEEENGGGGGGGGGGG');
              return buildBody();
            } else if (snapshot.connectionState == ConnectionState.waiting) {
              print('AYAAMMMMMM POOOOOOOOOOOOOOOPPPPPP');
              //_progressDialog.show();
              return Scaffold(
                body: Center(
                  child: Visibility(
                      visible: true,
                      child: Container(
                        height: 50,
                        width: 50,
                        child: CircularProgressIndicator(strokeWidth: 6),
                      )),
                ),
              );
            }
            return buildBody();
          }
      )
    );
  }
  Set<Circle> _circles = Set.from([
    Circle(
      circleId: CircleId('currentLocation'),
      center: LatLng(-6.2099059, 106.8355438), // Initial center at (0, 0)
      radius: 1000, // Initial radius of 1000 meters
      fillColor: Colors.blue.withOpacity(0.3), // Fill color with opacity
      strokeWidth: 0, // No border
    ),
  ]);

  buildBody(){
    return BlocConsumer<AbsenCubit,AbsenState>(
        listener: _listenerAbsen,
    builder: (context, state) {
      return BlocConsumer<VerifyFaceCubit,VerifyFaceState>(
        listener: _listenerVerifyFace,
        builder: (context,state){
          return WillPopScope(
            onWillPop: () async {
              if(state is AbsenLoading){
                print("cannot pop");
              } else {
                Navigator.pop(context);
              }
              return false;
            },
            child: Scaffold(
              floatingActionButton: CustomButtonConfirm(
                onTap: () {
                  if (state is! AbsenLoading) {
                    // Navigator.push(context, MaterialPageRoute(builder: (context) =>CameraStreamPage()));
                    // actionConfirmAttendance(context);
                    // _authenticate();
                    // onTakePictureButtonPressed();
                    // captureImage();
                    // if (controller.value.isStreamingImages) {
                    //     stopStreaming();
                    //   } else {
                    //     startStreaming();
                    //   }
                    //



                    Future.delayed(Duration(milliseconds: 500),(){
                      startImageCapture();
                    });
                    showModalBottomSheet(
                      context: context,
                      builder: (BuildContext context) {
                        return Container(
                          height: MediaQuery.of(context).size.height * 0.6,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 16),
                              Text("Validating Face..."),
                              SizedBox(height: 16),
                              Expanded(
                                child: Container(
                                  child: controller.value.isInitialized
                                      ? CameraPreview(controller)
                                      : CircularProgressIndicator(),
                                ),
                              ),
                              // SizedBox(height: 16),
                              // ElevatedButton(
                              //   onPressed: () {
                              //     startImageCapture();
                              //   },
                              //   child: isvalidating ? CircularProgressIndicator() : Text('Start Image Capture'),
                              // ),
                              SizedBox(height: 16),
                            ],
                          ),
                        );
                      },
                    );

                    // showModalBottomSheet(
                    //   context: context,
                    //   builder: (BuildContext context) {
                    //     return Container(
                    //       height: MediaQuery.of(context).size.height,
                    //       child: Column(
                    //         mainAxisAlignment: MainAxisAlignment.center,
                    //         children: <Widget>[
                    //           Expanded(
                    //             child: Container(
                    //               child: controller.value.isInitialized
                    //                   ? StreamBuilder<Image>(
                    //                 stream: imageStreamController.stream,
                    //                 builder: (context, snapshot) {
                    //                   if (snapshot.hasData) {
                    //                     return Column(
                    //                       children: [
                    //                         AspectRatio(
                    //                           aspectRatio: controller.value.aspectRatio,
                    //                           child: CameraPreview(controller),
                    //                         ),
                    //                         SizedBox(height: 20),
                    //                         snapshot.data,
                    //                         ElevatedButton(
                    //                           onPressed: () {
                    //                             // Upload the captured image file to the API
                    //                             uploadImageFile(File('/path/to/image.jpg'));
                    //                           },
                    //                           child: Text('Upload Image to API'),
                    //                         ),
                    //                       ],
                    //                     );
                    //                   } else {
                    //                     return AspectRatio(
                    //                       aspectRatio: controller.value.aspectRatio,
                    //                       child: CameraPreview(controller),
                    //                     );
                    //                   }
                    //                 },
                    //               )
                    //                   : Center(child: CircularProgressIndicator()),
                    //             ),
                    //           ),
                    //           ElevatedButton(
                    //             onPressed: () {
                    //               Navigator.pop(context);
                    //             },
                    //             child: Text('Close'),
                    //           ),
                    //         ],
                    //       ),
                    //     );
                    //   },
                    // );
                  }
                },
                title: "Input",
                isEnable: true,
                buttonColor: color_primary,
                textColor: Colors.white,
                isLoading: (state is AbsenLoading) ? true : false,
              ),
              body: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 8,),
                        Container(
                          margin: EdgeInsets.all(16),
                          child: IconButton(icon: Icon(Icons.arrow_back_rounded),
                              onPressed: () {
                                if(state is AbsenLoading){
                                  print("cannot pop");
                                } else {
                                  Navigator.pop(context);
                                }
                              }),
                        ),
                        Container(
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          margin: EdgeInsets.symmetric(horizontal: 16),
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(16),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 3,
                                offset: Offset(0, 0),
                              ),
                            ],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("$_timeString", style: TextStyle(fontSize: 24,
                                  color: color_primary,
                                  fontFamily: baseUrlFontsPoppinsSemiBold,
                                  fontWeight: FontWeight.bold),),
                              Text(_dateString, style: TextStyle(fontSize: 15,
                                  color: color_black,
                                  fontFamily: baseUrlFontsPoppinsSemiBold,
                                  fontWeight: FontWeight.bold),),
                            ],
                          ),
                        ),
                        Container(margin: EdgeInsets.all(16),
                            child: Text("Current Location", style: TextStyle(
                                fontSize: 17,
                                color: color_black,
                                fontFamily: baseUrlFontsPoppinsSemiBold,
                                fontWeight: FontWeight.bold),)),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          height: 200,
                          child: GoogleMap(
                            markers: Set<Marker>.of(_markersSBM.values),
                            zoomControlsEnabled: false,
                            rotateGesturesEnabled: false,
                            scrollGesturesEnabled: false,
                            zoomGesturesEnabled: false,
                            onMapCreated: _onMapCreated,
                            // circles: _circles,
                            initialCameraPosition: CameraPosition(
                              target: LatLng(_position.latitude,
                                  _position.longitude),
                              zoom: 17.6,
                            ),
                          ),
                        ),
                        SizedBox(height: 16,),
                        // !_isInitialized ? Center(child: CircularProgressIndicator()):CameraPreview(_controller),
                        SizedBox(height: 16,),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 16),
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          padding: EdgeInsets.symmetric(
                              horizontal: 16, vertical: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(16),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 3,
                                offset: Offset(0, 0),
                              ),
                            ],
                          ),
                          child: Row(
                            children: [
                              Icon(Icons.location_on),
                              SizedBox(width: 10,),
                              Expanded(child: Text(userAddress, maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontSize: 12,
                                    color: color_black,
                                    fontFamily: baseUrlFontsPoppinsSemiBold,
                                    fontWeight: FontWeight.bold),)),
                            ],
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(16),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 3,
                                offset: Offset(0, 0),
                              ),
                            ],
                          ),
                          margin: EdgeInsets.all(16),
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          child: DropdownButtonFormField(
                              icon: Icon(Icons.chevron_right),
                              style: TextStyle(
                                  fontFamily: baseUrlFontsPoppinsSemiBold,
                                  fontSize: 13,
                                  color: color_black),
                              decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.grey.shade300, width: 1),
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                filled: true,
                                fillColor: Colors.white,
                              ),
                              dropdownColor: Colors.white,
                              value: selectedValue,
                              onChanged: (newValue) {
                                setState(() {
                                  selectedValue = newValue;
                                  if (selectedValue == "present") {
                                    absenType = 1;
                                  } else {
                                    absenType = 2;
                                  }
                                });
                                _timeStringConfirm = formatTime(DateTime.now());
                                _dateStringConfirm = _formatDate2(DateTime.now());
                                print("Lokasi => "+userAddress);
                                print("Time => "+_timeStringConfirm);
                                print("date => "+_dateStringConfirm);
                                print("Type => "+absenType.toString());
                                print("Keterangan => "+notesController.text.toString());
                                print("Type Absen => "+selectedValue);
                                print("Type Absen => "+_position.latitude.toString());
                                print("Type Absen => "+_position.longitude.toString());
                              },


                              items: dropdownItems),
                        ),
                        // Container(
                        //   decoration: BoxDecoration(
                        //     borderRadius: BorderRadius.circular(16),
                        //     color: Colors.white,
                        //     boxShadow: [
                        //       BoxShadow(
                        //         color: Colors.grey.withOpacity(0.5),
                        //         spreadRadius: 1,
                        //         blurRadius: 3,
                        //         offset: Offset(0, 0),
                        //       ),
                        //     ],
                        //   ),
                        //   margin: EdgeInsets.only(left: 16, right: 16),
                        //   padding: EdgeInsets.all(16),
                        //   child: TextField(
                        //     controller: notesController,
                        //     style: TextStyle(
                        //         fontFamily: baseUrlFontsPoppinsRegular),
                        //     maxLines: 5,
                        //     maxLength: 150,
                        //     decoration: InputDecoration(
                        //         border: InputBorder.none,
                        //         hintText: "Berikan Keterangan (Opsional)"
                        //     ),
                        //     //onSubmitted: _getNotes(),
                        //   ),
                        // ),
                        SizedBox(
                          height: 16,
                        ),
                      ],
                    ),
                  )
              ),
            ),
          );
        }
      );
    }
    );
  }
}
