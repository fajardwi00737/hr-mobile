import 'package:absen_online/constant/text_constant.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert ;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';

part 'leave_reason_state.dart';

class LeaveReasonCubit extends Cubit<LeaveReasonState>{
  LeaveReasonCubit(): super(LeaveReasonInitial());

  void getLeaveReason()async{
    try{
      emit(LeaveReasonLoading());
      print("RESPONSE 123 "+GeneralSharedPreferences.readString("token_login"));

      http.Response response = await http.get(
          // 'http://192.168.1.130:81/api/v1/employee/list-leave',
          // 'http://172.20.10.5:81/api/v1/employee/list-leave',
          '${baseUrl}api/v1/employee/list-leave',
          headers: {
            'Authorization': 'Bearer ${GeneralSharedPreferences.readString("token_login")}',
          },
      );

      print("RESPONSE ${response.statusCode} ; BODY = ${response.body}");
      if(convert.jsonDecode(response.body)['success'] == true){
        emit(LeaveReasonSuccess(convert.jsonDecode(response.body)['data']));
      } else {
        emit(LeaveReasonFailed(msg: "failed"));
      }

    } catch (e){
      print("RESPONSE Error : ${e.toString()};");
      emit(LeaveReasonError(msg: e.toString()));
    }
  }
}