part of 'leave_reason_cubit.dart';


abstract class LeaveReasonState extends Equatable {
  const LeaveReasonState();

  @override
  List<Object> get props => [];
}

class LeaveReasonInitial extends LeaveReasonState{}

class LeaveReasonLoading extends LeaveReasonState{
  const LeaveReasonLoading();
}

class LeaveReasonSuccess extends LeaveReasonState{
  final Map<String, dynamic> dataLeaveReason;

  LeaveReasonSuccess(this.dataLeaveReason);

  @override
  List<Object> get props => [dataLeaveReason];
}

class LeaveReasonFailed extends LeaveReasonState {
  final String msg;

  LeaveReasonFailed({this.msg});
}

class LeaveReasonError extends LeaveReasonState {
  final String msg;

  LeaveReasonError({this.msg});
}