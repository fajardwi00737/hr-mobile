import 'package:absen_online/constant/text_constant.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert ;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';

part 'payroll_state.dart';

class PayrollCubit extends Cubit<PayrollState>{
  PayrollCubit(): super(PayrollInitial());
  
  void getPayroll(String workingPeriod, String payrollPeriod)async{
    try{
      emit(PayrollLoading());
      print("RESPONSE 123 "+GeneralSharedPreferences.readString("token_login"));
      Map<String, dynamic>    formMap = {
        "working_period": workingPeriod,
        "payroll_period": payrollPeriod
      };

      http.Response response = await http.post(
        // 'http://192.168.1.130:81/api/v1/employee/payroll-filter-employee',
        // 'http://172.20.10.5:81/api/v1/employee/payroll-filter-employee',
        // 'http://192.168.1.133:81/api/v1/employee/payroll-filter-employee',
        '${baseUrl}api/v1/employee/payroll-month',
        body: formMap,
        headers: {
          'Authorization': 'Bearer ${GeneralSharedPreferences.readString("token_login")}',
        },
        encoding: convert.Encoding.getByName("utf-8")
      );

      print("RESPONSE ${response.statusCode} ; BODY = ${response.body}");
      // print("RESPONSE "+convert.jsonDecode(response.body)['data']['employee']['get_salary_configuration']["basic_salary"]);
      if(convert.jsonDecode(response.body)['success'] == true){
        emit(PayrollSuccess(convert.jsonDecode(response.body)['data']));
      } else {
        emit(PayrollFailed(msg: "failed"));
      }

    } catch (e){
      print("RESPONSE Error : ${e.toString()};");
      emit(PayrollError(msg: e.toString()));
    }
  }
}