part of 'payroll_cubit.dart';


abstract class PayrollState extends Equatable {
  const PayrollState();

  @override
  List<Object> get props => [];
}

class PayrollInitial extends PayrollState{}

class PayrollLoading extends PayrollState{
  const PayrollLoading();
}

class PayrollSuccess extends PayrollState{
  final Map<String, dynamic> dataPayroll;

  PayrollSuccess(this.dataPayroll);

  @override
  List<Object> get props => [dataPayroll];
}

class PayrollFailed extends PayrollState {
  final String msg;

  PayrollFailed({this.msg});
}

class PayrollError extends PayrollState {
  final String msg;

  PayrollError({this.msg});
}