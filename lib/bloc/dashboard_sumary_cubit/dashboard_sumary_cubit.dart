import 'package:absen_online/constant/text_constant.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert ;

part 'dashboard_sumary_state.dart';

class DashboardSumaryCubit extends Cubit<DashboardSumaryState>{
  DashboardSumaryCubit() :super(DashboardSumaryInitial());

  void getDashboardSumary()async{
    try{
      emit(DashboardSumaryLoading());
      http.Response response = await http.get(
        // 'https://api.simerahputih.com/absen/user/summary',
        // 'http://47.243.59.72:3024/absen/user/summary',
        // 'http://172.20.10.5:81/api/v1/employee/dashboard',
        '${baseUrl}api/v1/employee/dashboard',
        headers: {
          'Authorization': 'Bearer ${GeneralSharedPreferences.readString("token_login")}',
        },
      );
      // print("success get data => "+convert.jsonDecode(response.body)['data']['count_cuti']);

      if(convert.jsonDecode(response.body)['success'] == true){

        emit(DashboardSumarySuccess(convert.jsonDecode(response.body)['data']));
      } else {
        print("failed get data dashboard");
        emit(DashboardSumaryFailed(msg :convert.jsonDecode(response.body)['message']));
      }
    } catch (e){
      emit(DashboardSumaryError(msg: e.toString()));
    }
  }
}