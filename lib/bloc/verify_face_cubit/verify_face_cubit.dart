import 'dart:io';

import 'package:absen_online/constant/text_constant.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert ;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';

part 'verify_face_state.dart';

class VerifyFaceCubit extends Cubit<VerifyFaceState>{
  VerifyFaceCubit(): super(VerifyFaceInitial());

  void verifyFace(File facePhoto) async{
    try{
      VerifyFaceLoading();

      var request = http.MultipartRequest('POST', Uri.parse('${baseUrl}api/v1/employee/verify-face'));
      request.files.add(
          http.MultipartFile(
              'face_photo',
              facePhoto.readAsBytes().asStream(),
              facePhoto.lengthSync(),
              filename: facePhoto.path.split("/").last
          )
      );
      request.headers['Authorization'] = 'Bearer ${GeneralSharedPreferences.readString("token_login")}';
      var res = await request.send();


      print("RESPONSE ${res.stream} ; BODY = ${res.statusCode} ; BODY = ${res.toString()}");
      // Map<String, dynamic>    formMap = {
      //   "face_photo": facePhoto
      // };
      //
      // http.Response response = await http.post(
      //     '${baseUrl}api/v1/employee/verify-face',
      //     body: formMap,
      //     headers: {
      //       'Authorization': 'Bearer ${GeneralSharedPreferences.readString("token_login")}',
      //     },
      //     encoding: convert.Encoding.getByName("utf-8")
      // );

      // print("RESPONSE ${response.statusCode} ; BODY = ${response.body}");
      var responseString = await res.stream.bytesToString();
      // print("RESPONSE "+convert.jsonDecode(response.body)['data']['employee']['get_salary_configuration']["basic_salary"]);
      if(convert.jsonDecode(responseString)['data']['status_code'] == "200"){
        emit(VerifyFaceSuccess(convert.jsonDecode(responseString)));
      } else {
        emit(VerifyFaceFailed(msg: "Face Not Verified"));
      }

    }catch (e){
      emit(VerifyFaceError(msg: e.toString()));
    }
  }
}