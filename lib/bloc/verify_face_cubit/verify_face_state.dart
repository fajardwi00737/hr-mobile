part of 'verify_face_cubit.dart';

abstract class VerifyFaceState extends Equatable{
  const VerifyFaceState();

  @override
  List<Object> get props => [];
}

class VerifyFaceInitial extends VerifyFaceState{}
class VerifyFaceLoading extends VerifyFaceState{
  const VerifyFaceLoading();
}
class VerifyFaceSuccess extends VerifyFaceState{
  final Map<String, dynamic> dataPayroll;

  VerifyFaceSuccess(this.dataPayroll);

  @override
  List<Object> get props => [dataPayroll];
}
class VerifyFaceFailed extends VerifyFaceState{
  final String msg;

  VerifyFaceFailed({this.msg});
}
class VerifyFaceError extends VerifyFaceState{
  final String msg;

  VerifyFaceError({this.msg});
}