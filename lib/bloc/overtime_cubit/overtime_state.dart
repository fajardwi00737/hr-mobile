part of 'overtime_cubit.dart';

abstract class OvertimeState extends Equatable{
  const OvertimeState();

  @override
  List<Object> get props => [];
}

class OvertimeInitial extends OvertimeState{}
class OvertimeLoading extends OvertimeState{
  const OvertimeLoading();
}
class OvertimeSuccess extends OvertimeState{
  final String msg;
  OvertimeSuccess(this.msg);
}
class OvertimeFailed extends OvertimeState{
  final String msg;

  OvertimeFailed({this.msg});
}
class OvertimeError extends OvertimeState{
  final String msg;

  OvertimeError({this.msg});
}