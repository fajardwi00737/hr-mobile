import 'package:absen_online/constant/text_constant.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert ;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';

part 'overtime_state.dart';

class OvertimeCubit extends Cubit<OvertimeState>{
  OvertimeCubit(): super(OvertimeInitial());

  void confirmOvertime(String overtimeType,int overtimeAmount, String overtimeDate, String overtimeTax, overtimeReason)async{
    try{
      emit(OvertimeLoading());
      Map<String, dynamic>    formMap = {
        "overtime_type": overtimeType,
        "month_year": overtimeDate,
        "reason": overtimeReason,
        "hour": overtimeType == "Hours" ? overtimeAmount.toString():"0",
        "day": overtimeType == "Days" ? overtimeAmount.toString():"0",
        "tax_overtime": overtimeTax,
      };

      http.Response response = await http.post(
          '${baseUrl}api/v1/employee/store-overtime',
          body: formMap,
          headers: {
            'Authorization': 'Bearer ${GeneralSharedPreferences.readString("token_login")}',
          },
          encoding: convert.Encoding.getByName("utf-8")
      );

      print("RESPONSE ${response.statusCode} ; BODY = ${response.body}");
      // print("RESPONSE "+convert.jsonDecode(response.body)['data']['employee']['get_salary_configuration']["basic_salary"]);
      if(convert.jsonDecode(response.body)['success'] == true){
        emit(OvertimeSuccess(convert.jsonDecode(response.body)['message']));
      } else {
        emit(OvertimeFailed(msg: convert.jsonDecode(response.body)['message']));
      }
    }catch (e){
      print("RESPONSE Error : ${e.toString()};");
      emit(OvertimeError(msg: e.toString()));
    }
  }
}