part of 'employee_cubit.dart';


abstract class EmployeeState extends Equatable {
  const EmployeeState();

  @override
  List<Object> get props => [];
}

class EmployeeInitial extends EmployeeState{}

class EmployeeLoading extends EmployeeState{
  const EmployeeLoading();
}

class EmployeeSuccess extends EmployeeState{
  final Map<String, dynamic> dataEmployee;

  EmployeeSuccess(this.dataEmployee);

  @override
  List<Object> get props => [dataEmployee];
}

class EmployeeFailed extends EmployeeState {
  final String msg;

  EmployeeFailed({this.msg});
}

class EmployeeError extends EmployeeState {
  final String msg;

  EmployeeError({this.msg});
}