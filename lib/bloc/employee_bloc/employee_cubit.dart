import 'package:absen_online/constant/text_constant.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert ;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';

part 'employee_state.dart';

class EmployeeCubit extends Cubit<EmployeeState>{
  EmployeeCubit(): super(EmployeeInitial());

  void getEmployee()async{
    try{
      emit(EmployeeLoading());
      print("RESPONSE 123 "+GeneralSharedPreferences.readString("token_login"));

      http.Response response = await http.get(
          // 'http://192.168.1.130:81/api/v1/employee/list',
          // 'http://172.20.10.5:81/api/v1/employee/list',
          '${baseUrl}api/v1/employee/list',
          headers: {
            'Authorization': 'Bearer ${GeneralSharedPreferences.readString("token_login")}',
          },
      );

      print("RESPONSE ${response.statusCode} ; BODY = ${response.body}");
      print("RESPONSE "+convert.jsonDecode(response.body)['data']['employee'].toString());
      if(convert.jsonDecode(response.body)['success'] == true){
        emit(EmployeeSuccess(convert.jsonDecode(response.body)['data']));
      } else {
        emit(EmployeeFailed(msg: convert.jsonDecode(response.body)['message']));
      }

    } catch (e){
      print("RESPONSE Error : ${e.toString()};");
      emit(EmployeeError(msg: e.toString()));
    }
  }
}