import 'package:absen_online/constant/text_constant.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert ;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';

part 'leave_detail_state.dart';

class LeaveDetailCubit extends Cubit<LeaveDetailState>{
  LeaveDetailCubit(): super(LeaveDetailInitial());

  void getLeaveDetail(int leaveId)async{
    try{
      print("Leave id : $leaveId;");

      Map<String, dynamic> formMap = {
        "leave_id": leaveId.toString(),
      };

      http.Response response = await http.post(
        '${baseUrl}api/v1/employee/leave-log',
        body: formMap,
        headers: {
          'Authorization': 'Bearer ${GeneralSharedPreferences.readString("token_login")}',
        },
        encoding: convert.Encoding.getByName("utf-8"),
      );
      print("RESPONSE ${response.statusCode} ; BODY = ${response.body}");
      if(convert.jsonDecode(response.body)['success'] == true){
        emit(LeaveDetailSuccess(convert.jsonDecode(response.body)));
      } else {
        emit(LeaveDetailFailed(msg: "failed"));
      }
    } catch (e){
      print("RESPONSE Error : ${e.toString()};");
      emit(LeaveDetailError(msg: e.toString()));
    }
  }
}