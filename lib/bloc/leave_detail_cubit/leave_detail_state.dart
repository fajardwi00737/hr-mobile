part of 'leave_detail_cubit.dart';

abstract class LeaveDetailState extends Equatable {
  const LeaveDetailState();

  @override
  List<Object> get props => [];
}

class LeaveDetailInitial extends LeaveDetailState{}

class LeaveDetailLoading extends LeaveDetailState{}

class LeaveDetailSuccess extends LeaveDetailState{
  final Map<String, dynamic> dataLeaveDetail;

  LeaveDetailSuccess(this.dataLeaveDetail);

  @override
  List<Object> get props => [dataLeaveDetail];
}

class LeaveDetailFailed extends LeaveDetailState {
  final String msg;

  LeaveDetailFailed({this.msg});
}

class LeaveDetailError extends LeaveDetailState {
  final String msg;

  LeaveDetailError({this.msg});
}