import 'package:absen_online/constant/text_constant.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert ;

part 'absen_state.dart';

class AbsenCubit extends Cubit<AbsenState>{
  AbsenCubit() :super(AbsenInitial());

  void actionAbsen(String userId, String date, String time, String type, String location,double lat,double long)async{

    print("lats => "+lat.toString());
    print("lats => "+long.toString());
    try{
      emit(AbsenLoading());
      Map<String, dynamic>    formMap = {
        "attendance_date": date,
        "clock_attendance": time,
        "attendace_location": location,
        "attendance_status": type,
        "latitude": lat.toString(),
        "longitude": long.toString(),
      };

      http.Response response = await http.post(
        // 'https://api.simerahputih.com/absen/user/presence',
        // 'http://47.243.59.72:3024/absen/user/presence',
        // 'http://192.168.1.130:81/api/v1/employee/store-attendance',
        // 'http://172.20.10.5:81/api/v1/employee/store-attendance',
        '${baseUrl}api/v1/employee/store-attendance',
        body: formMap,
        headers: {
          'Authorization': 'Bearer ${GeneralSharedPreferences.readString("token_login")}',
        },
        encoding: convert.Encoding.getByName("utf-8"),
      );
      print("RESPONSE ${response.statusCode} ; BODY = ${response.body}");
      if(convert.jsonDecode(response.body)['success'] == true){
        print("success Absen");
        if(type == "present"){
          GeneralSharedPreferences.writeString("clock_in", time);
          GeneralSharedPreferences.writeBool("is_clock_in", true);
        }else {
          GeneralSharedPreferences.writeString("clock_out", time);
          GeneralSharedPreferences.writeBool("is_clock_out", true);
        }

        emit(AbsenSucces(convert.jsonDecode(response.body)['message']));
      } else {
        print("gagal Absen");
        emit(AbsenFailed(convert.jsonDecode(response.body)['message']));
      }

    }catch (e){
      emit(AbsenError(msg: e.toString()));
    }
  }
}