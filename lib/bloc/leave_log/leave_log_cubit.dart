import 'package:absen_online/constant/text_constant.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert ;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';

part 'leave_log_state.dart';

class LeaveLogCubit extends Cubit<LeaveLogState>{
  LeaveLogCubit(): super(LeaveLogInitial());

  void getLeaveLog(int idLeave)async{
    try{

      Map<String, dynamic> formMap = {
        "leave_id": idLeave,
      };

      emit(LeaveLogLoading());
      print("RESPONSE 123 "+GeneralSharedPreferences.readString("token_login"));

      http.Response response = await http.post(
        // 'http://192.168.1.130:81/api/v1/employee/list-leave',
        // 'http://172.20.10.5:81/api/v1/employee/list-leave',
        '${baseUrl}api/v1/employee/leave-log',
        body: formMap,
        headers: {
          'Authorization': 'Bearer ${GeneralSharedPreferences.readString("token_login")}',
        },
          encoding: convert.Encoding.getByName("utf-8")
      );

      print("RESPONSE ${response.statusCode} ; BODY = ${response.body}");
      if(convert.jsonDecode(response.body)['success'] == true){
        emit(LeaveLogSuccess(convert.jsonDecode(response.body)['data']));
      } else {
        emit(LeaveLogFailed(msg: "failed"));
      }

    } catch (e){
      print("RESPONSE Error : ${e.toString()};");
      emit(LeaveLogError(msg: e.toString()));
    }
  }
}