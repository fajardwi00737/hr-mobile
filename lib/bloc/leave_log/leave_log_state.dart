part of 'leave_log_cubit.dart';


abstract class LeaveLogState extends Equatable {
  const LeaveLogState();

  @override
  List<Object> get props => [];
}

class LeaveLogInitial extends LeaveLogState{}

class LeaveLogLoading extends LeaveLogState{
  const LeaveLogLoading();
}

class LeaveLogSuccess extends LeaveLogState{
  final Map<String, dynamic> dataLeaveLog;

  LeaveLogSuccess(this.dataLeaveLog);

  @override
  List<Object> get props => [dataLeaveLog];
}

class LeaveLogFailed extends LeaveLogState {
  final String msg;

  LeaveLogFailed({this.msg});
}

class LeaveLogError extends LeaveLogState {
  final String msg;

  LeaveLogError({this.msg});
}