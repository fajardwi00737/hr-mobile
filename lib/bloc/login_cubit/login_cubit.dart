import 'package:absen_online/constant/text_constant.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert ;

import '../../main.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState>{
  LoginCubit() :super(LoginInitial());


  void login(String email, String password,String fcmToken)async{
    try{
      emit(LoginLoading());

      // client.login(phone, password).then((value){
      Map<String, dynamic>    formMap = {
        "email": email,
        "password": password
      };

      http.Response response = await http.post(
        // 'https://api.simerahputih.com/absen/user/login',
        // 'http://192.168.1.130:81/absen/user/login',
        // 'http://192.168.1.130:81/api/v1/login',
        // 'http://172.20.10.5:81/api/v1/login',
        '${baseUrl}api/v1/login',
        body: formMap,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        encoding: convert.Encoding.getByName("utf-8"),
      );
      print("RESPONSE ${response.statusCode} ; BODY = ${response.body}");
      // print("21as "+convert.jsonDecode(response.body)['meta']['code'].toString());
      // print("21as "+convert.jsonDecode(response.body)['meta']['message']);
      // if(convert.jsonDecode(response.body)['success'] == true){
      if(response.statusCode == 200){
        print("success");
        GeneralSharedPreferences.writeString("token_login", convert.jsonDecode(response.body)['data']['Authorization']['token']);
        GeneralSharedPreferences.writeBool("is_login", true);
        GeneralSharedPreferences.writeInt("user_id", convert.jsonDecode(response.body)['data']['user']['id']);
        GeneralSharedPreferences.writeString("user_name", convert.jsonDecode(response.body)['data']['user']['first_name']+" "+convert.jsonDecode(response.body)['data']['user']['last_name']);
        GeneralSharedPreferences.writeString("user_mail", convert.jsonDecode(response.body)['data']['user']['email']);
        GeneralSharedPreferences.writeString("user_phone", convert.jsonDecode(response.body)['data']['user']['contact_no']);
        GeneralSharedPreferences.writeString("user_address", "jalan Guntur no 68");
        GeneralSharedPreferences.writeString("fcm_token", "awdhiawdh");

        // http.Response response2 = await http.get(
        //   // 'https://api.simerahputih.com/absen/admin/detail_divisi?id='+convert.jsonDecode(response.body)['data'][0]['divisi_id'].toString(),
        //   'http://47.243.59.72:3024/absen/admin/detail_divisi?id='+convert.jsonDecode(response.body)['data'][0]['divisi_id'].toString(),
        //   headers: {
        //     "x-api-key" : GeneralSharedPreferences.readString("token_login")
        //   },
        // );

        GeneralSharedPreferences.writeString("user_divisi", "IT Dev");
        emit(LoginSucces());
      } else {
        emit(LoginFailed(convert.jsonDecode(response.body)['error']));
        print("gagal");
      }
      // });
    }catch(e){
      emit(LoginError(msg: e.toString()));
    }
  }
}