import 'package:absen_online/constant/text_constant.dart';
import 'package:absen_online/utils/general_shared_preferences/general_shared_preferences.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert ;

part 'cuti_state.dart';

class CutiCubit extends Cubit<CutiState>{
  CutiCubit() :super(CutiInitial());

  void actionCuti(int type,String startDate, String endtDate, String reason)async{
    try{
      emit(CutiLoading());
      Map<String, dynamic>    formMap = {
        "leave_type": type.toString(),
        "status": "pending",
        "start_date": startDate,
        "end_date": endtDate,
        "leave_reason": reason,
        "remarks": "",
      };

      http.Response response = await http.post(
        // 'https://api.simerahputih.com/absen/user/cuti',
        // 'http://47.243.59.72:3024/absen/user/cuti',
        // 'http://192.168.1.130:81/api/v1/employee/store-leave',
        // 'http://172.20.10.5:81/api/v1/employee/store-leave',
        '${baseUrl}api/v1/employee/store-leave',
        body: formMap,
        headers: {
          'Authorization': 'Bearer ${GeneralSharedPreferences.readString("token_login")}',
        },
        encoding: convert.Encoding.getByName("utf-8"),
      );
      print("RESPONSE ${response.statusCode} ; BODY = ${response.body}");
      if(convert.jsonDecode(response.body)['success'] == true){
        print("success Cuti");
        emit(CutiSucces(convert.jsonDecode(response.body)['message']));
      } else {
        print("gagal Cuti");
        emit(CutiFailed(convert.jsonDecode(response.body)['message']));
      }

    }catch (e){
      emit(CutiError(msg: e.toString()));
    }
  }
}