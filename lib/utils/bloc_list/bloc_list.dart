
import 'package:absen_online/bloc/absen_cubit/absen_cubit.dart';
import 'package:absen_online/bloc/cuti_cubit/cuti_cubit.dart';
import 'package:absen_online/bloc/cuti_list_cubit/cuti_list_cubit.dart';
import 'package:absen_online/bloc/dashboard_sumary_cubit/dashboard_sumary_cubit.dart';
import 'package:absen_online/bloc/employee_bloc/employee_cubit.dart';
import 'package:absen_online/bloc/inbox_cubit/inbox_cubit.dart';
import 'package:absen_online/bloc/leave_cubit/leave_reason_cubit.dart';
import 'package:absen_online/bloc/leave_detail_cubit/leave_detail_cubit.dart';
import 'package:absen_online/bloc/leave_log/leave_log_cubit.dart';
import 'package:absen_online/bloc/login_cubit/login_cubit.dart';
import 'package:absen_online/bloc/overtime_cubit/overtime_cubit.dart';
import 'package:absen_online/bloc/payroll_cubit/payroll_cubit.dart';
import 'package:absen_online/bloc/presence_list_bloc/presence_list_cubit.dart';
import 'package:absen_online/bloc/verify_face_cubit/verify_face_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

final List<BlocProvider> blocList = [

  BlocProvider<AbsenCubit>(create: (_) => AbsenCubit()),
  BlocProvider<CutiCubit>(create: (_) => CutiCubit()),
  BlocProvider<LoginCubit>(create: (_) => LoginCubit()),
  BlocProvider<PresenceListCubit>(create: (_) => PresenceListCubit()),
  BlocProvider<CutiListCubit>(create: (_) => CutiListCubit()),
  BlocProvider<DashboardSumaryCubit>(create: (_) => DashboardSumaryCubit()),
  BlocProvider<InboxCubit>(create: (_) => InboxCubit()),
  BlocProvider<PayrollCubit>(create: (_) => PayrollCubit()),
  BlocProvider<EmployeeCubit>(create: (_) => EmployeeCubit()),
  BlocProvider<LeaveReasonCubit>(create: (_) => LeaveReasonCubit()),
  BlocProvider<LeaveLogCubit>(create: (_) => LeaveLogCubit()),
  BlocProvider<VerifyFaceCubit>(create: (_) => VerifyFaceCubit()),
  BlocProvider<OvertimeCubit>(create: (_) => OvertimeCubit()),
];